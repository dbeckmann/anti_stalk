package com.dbeckmann.rxexecutor

import android.os.Looper
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.ExecutorCallback
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.SingleOnSubscribe
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver

/**
 * Created by daniel on 25.02.2018.
 */

open class RxExecutor(
        val subscribeScheduler: Scheduler,
        private val observeScheduler: Scheduler,
        private var identifier: String
) : UseCaseExecutor {

    private var disposables: CompositeDisposable = CompositeDisposable()

    companion object {
        private const val TAG = "RxExecutor"
    }

    init {
        identifier = "$identifier ${this.hashCode()}"
        println("$TAG: created ($identifier)")
    }

    override fun<Result> run(executor: Executor<Result>, callback: UseCaseCallback<Result>) {
        println("$TAG run ($executor) ($identifier)")

        val disposable = createSingle(executor)
                .observeOn(observeScheduler)
                .subscribeWith(object : DisposableSingleObserver<Result>() {
                    override fun onSuccess(t: Result) {
                        callback.onSuccess(t)
                    }

                    override fun onError(e: Throwable) {
                        callback.onError(e)
                    }
                })

        disposables.add(disposable)
    }

    override fun<Result> runBlocking(executor: Executor<Result>, callback: UseCaseCallback<Result>) {
        try {

            if (Looper.getMainLooper().thread == Thread.currentThread()) {
                println("$TAG: warning ! blocking operation on main thread (${executor.javaClass.name}) ($identifier)")
            }

            callback.onSuccess(createSingle(executor)
                    .blockingGet())
        } catch (e: Exception) {
            callback.onError(e)
        }
    }

    override fun stop() {
        println("$TAG: stopping all running executors ($identifier)")
        disposables.clear()
    }

    override fun isDestroyed(): Boolean = false

    fun<Result> createSingle(executor: Executor<Result>) : Single<Result> {
        return Single.create(SingleOnSubscribe<Result> { e ->
            executor.execute(object : ExecutorCallback<Result> {
                override fun onSuccess(result: Result) = e.onSuccess(result)
                override fun onError(throwable: Throwable) = e.onError(throwable)
            })
        })
                .subscribeOn(subscribeScheduler)
    }
}