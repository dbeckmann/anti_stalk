package com.dbeckmann.interactor.usecase

/**
 * Created by daniel on 24.02.2018.
 */
interface Executor<out T> {
    fun execute(callback: ExecutorCallback<T>)

    companion object {
        operator fun <T> invoke(fn: (ExecutorCallback<T>) -> Unit) = object : Executor<T> {
            override fun execute(callback: ExecutorCallback<T>) = fn(callback)
        }
    }
}

interface ExecutorCallback<in T> {
    fun onSuccess(result: T)
    fun onError(throwable: Throwable)

    companion object {
        operator fun <Result> invoke(success: (Result) -> Unit, error: (Throwable) -> Unit) = object : ExecutorCallback<Result> {
            override fun onSuccess(result: Result) = success(result)
            override fun onError(throwable: Throwable) = error(throwable)
        }
    }
}