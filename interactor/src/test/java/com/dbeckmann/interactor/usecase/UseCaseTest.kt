package com.dbeckmann.interactor.usecase

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UseCaseTest {

    private val testUseCaseExecutor = TestUseCaseExecutor()
    private val testExecutor = TestExecutor()
    private val testUseCase = TestUseCase(testUseCaseExecutor, testExecutor)

    /**
     * execute functions
     */

    @Test
    fun executeParamsAndCallback_successResult_onSuccessCalled() {
        val callback: UseCaseCallback<Any> = mock()

        // set the result returned by executor
        testExecutor.result = 1000
        testUseCase.execute(testExecutor.result!!, callback)

        verify(callback, times(1)).onSuccess(any())
        verify(callback, times(0)).onError(any())
    }

    @Test
    fun executeParamsAndCallback_errorResult_onErrorCalled() {
        val callback: UseCaseCallback<Any> = mock()

        // null will return onError as result
        testExecutor.result = null
        testUseCase.execute(1000, callback)

        verify(callback, times(0)).onSuccess(any())
        verify(callback, times(1)).onError(any())
    }

    @Test
    fun executeParamsOnly_isExecuted_executorExecuteCalled() {
        val mockExecutor: Executor<Any> = mock()
        val mockUseCase = TestUseCase(testUseCaseExecutor, mockExecutor)

        mockUseCase.execute(1000)

        verify(mockExecutor).execute(any())
    }

    @Test
    fun executeParamsAndFunctionCallback_successResult_onSuccessCalled() {
        val mockSuccess: (Any) -> Unit = mock()
        val mockError: (Throwable) -> Unit = mock()

        // set the result returned by executor
        testExecutor.result = 1000
        testUseCase.execute(1000, mockSuccess, mockError)

        verify(mockSuccess, times(1)).invoke(any())
        verify(mockError, times(0)).invoke(any())
    }

    @Test
    fun executeParamsAndFunctionCallbacks_errorResult_onErrorCalled() {
        val mockSuccess: (Any) -> Unit = mock()
        val mockError: (Throwable) -> Unit = mock()

        // null will return onError as result
        testExecutor.result = null
        testUseCase.execute(1000, mockSuccess, mockError)

        verify(mockSuccess, times(0)).invoke(any())
        verify(mockError, times(1)).invoke(any())
    }

    @Test
    fun executeParamsAndSuccessFunctionCallback_successResult_onSuccessCalled() {
        val mockSuccess: (Any) -> Unit = mock()

        // set the result returned by executor
        testExecutor.result = 1000
        testUseCase.execute(1000, mockSuccess)

        verify(mockSuccess, times(1)).invoke(any())
    }

    @Test
    fun executeParamsAndSuccessFunctionCallback_errorResult_successFunctionNotInvoked() {
        val mockSuccess: (Any) -> Unit = mock()

        testExecutor.result = null
        testUseCase.execute(1000, mockSuccess)

        verify(mockSuccess, times(0)).invoke(any())
    }

    /**
     * executeBlocking functions
     */

    @Test
    fun executeBlockingParamsAndCallback_successResult_onSuccessCalled() {
        val callback: UseCaseCallback<Any> = mock()

        // set the result returned by executor
        testExecutor.result = 1000
        testUseCase.executeBlocking(testExecutor.result!!, callback)

        verify(callback, times(1)).onSuccess(any())
        verify(callback, times(0)).onError(any())
    }

    @Test
    fun executeBlockingParamsAndCallback_errorResult_onErrorCalled() {
        val callback: UseCaseCallback<Any> = mock()

        // null will return onError as result
        testExecutor.result = null
        testUseCase.executeBlocking(1000, callback)

        verify(callback, times(0)).onSuccess(any())
        verify(callback, times(1)).onError(any())
    }

    @Test
    fun executeBlockingParamsOnly_isExecuted_executorExecuteCalled() {
        val mockExecutor: Executor<Any> = mock()
        val mockUseCase = TestUseCase(testUseCaseExecutor, mockExecutor)

        mockUseCase.executeBlocking(1000)

        verify(mockExecutor).execute(any())
    }

    @Test
    fun executeBlockingParamsAndFunctionCallback_successResult_onSuccessCalled() {
        val mockSuccess: (Any) -> Unit = mock()
        val mockError: (Throwable) -> Unit = mock()

        // set the result returned by executor
        testExecutor.result = 1000
        testUseCase.executeBlocking(1000, mockSuccess, mockError)

        verify(mockSuccess, times(1)).invoke(any())
        verify(mockError, times(0)).invoke(any())
    }

    @Test
    fun executeBlockingParamsAndFunctionCallbacks_errorResult_onErrorCalled() {
        val mockSuccess: (Any) -> Unit = mock()
        val mockError: (Throwable) -> Unit = mock()

        // null will return onError as result
        testExecutor.result = null
        testUseCase.executeBlocking(1000, mockSuccess, mockError)

        verify(mockSuccess, times(0)).invoke(any())
        verify(mockError, times(1)).invoke(any())
    }

    @Test
    fun executeBlockingParamsAndSuccessFunctionCallback_successResult_onSuccessCalled() {
        val mockSuccess: (Any) -> Unit = mock()

        // set the result returned by executor
        testExecutor.result = 1000
        testUseCase.executeBlocking(1000, mockSuccess)

        verify(mockSuccess, times(1)).invoke(any())
    }

    @Test
    fun executeBlockingParamsAndSuccessFunctionCallback_errorResult_successFunctionNotInvoked() {
        val mockSuccess: (Any) -> Unit = mock()

        testExecutor.result = null
        testUseCase.executeBlocking(1000, mockSuccess)

        verify(mockSuccess, times(0)).invoke(any())
    }
}