package com.dbeckmann.interactor.usecase

open class TestUseCase(
    useCaseExecutor: UseCaseExecutor,
    open val executor: Executor<Any>
) : UseCase<Any, Any>(useCaseExecutor) {
    override fun buildUseCase(params: Any): Executor<Any> {
        return executor
    }
}