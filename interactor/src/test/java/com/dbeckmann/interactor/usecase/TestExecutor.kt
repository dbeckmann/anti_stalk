package com.dbeckmann.interactor.usecase

class TestExecutor : Executor<Any> {
    var result: Any? = null

    override fun execute(callback: ExecutorCallback<Any>) {
        result?.also {
            callback.onSuccess(it)
        } ?: callback.onError(NoSuchElementException("expected a result to be returned"))
    }
}