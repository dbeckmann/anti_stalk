package com.dbeckmann.interactor.usecase

class TestUseCaseExecutor : UseCaseExecutor {
    override fun <Result> run(executor: Executor<Result>, callback: UseCaseCallback<Result>) {
        executor.execute(
            ExecutorCallback.invoke(
                {
                    callback.onSuccess(it)
                },
                {
                    callback.onError(it)
                })
        )
    }

    override fun <Result> runBlocking(executor: Executor<Result>, callback: UseCaseCallback<Result>) {
        run(executor, callback)
    }

    override fun stop() {
        // not implemented here
    }

    override fun isDestroyed(): Boolean = false
}