package com.dbeckmann.interactor.usecase

open class TestNoParamsUseCase(
    useCaseExecutor: UseCaseExecutor,
    open val executor: Executor<Any>
) : NoParamUseCase<Any>(useCaseExecutor) {
    override fun buildUseCase(): Executor<Any> {
        return executor
    }
}