package com.dbeckmann.interactor.usecase

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class NoParamsUseCaseTest {

    private val testUseCaseExecutor = TestUseCaseExecutor()
    private val testExecutor = TestExecutor()
    private val testUseCase = TestNoParamsUseCase(testUseCaseExecutor, testExecutor)

    /**
     * execute functions
     */

    @Test
    fun executeCallback_successResult_onSuccessCalled() {
        val callback: UseCaseCallback<Any> = mock()

        // set the result returned by executor
        testExecutor.result = 1000
        testUseCase.execute(callback)

        verify(callback, times(1)).onSuccess(any())
        verify(callback, times(0)).onError(any())
    }

    @Test
    fun executeCallback_errorResult_onErrorCalled() {
        val callback: UseCaseCallback<Any> = mock()

        // null will return onError as result
        testExecutor.result = null
        testUseCase.execute(callback)

        verify(callback, times(0)).onSuccess(any())
        verify(callback, times(1)).onError(any())
    }

    @Test
    fun execute_isExecuted_executorExecuteCalled() {
        val mockExecutor: Executor<Any> = mock()
        val mockUseCase = TestNoParamsUseCase(testUseCaseExecutor, mockExecutor)

        mockUseCase.execute()

        verify(mockExecutor).execute(any())
    }

    @Test
    fun executeFunctionCallbacks_successResult_onSuccessCalled() {
        val mockSuccess: (Any) -> Unit = mock()
        val mockError: (Throwable) -> Unit = mock()

        // set the result returned by executor
        testExecutor.result = 1000
        testUseCase.execute(mockSuccess, mockError)

        verify(mockSuccess, times(1)).invoke(any())
        verify(mockError, times(0)).invoke(any())
    }

    @Test
    fun executeFunctionCallbacks_errorResult_onErrorCalled() {
        val mockSuccess: (Any) -> Unit = mock()
        val mockError: (Throwable) -> Unit = mock()

        // null will return onError as result
        testExecutor.result = null
        testUseCase.execute(mockSuccess, mockError)

        verify(mockSuccess, times(0)).invoke(any())
        verify(mockError, times(1)).invoke(any())
    }

    @Test
    fun executeSuccessFunctionCallback_successResult_onSuccessCalled() {
        val mockSuccess: (Any) -> Unit = mock()

        // set the result returned by executor
        testExecutor.result = 1000
        testUseCase.execute(mockSuccess)

        verify(mockSuccess, times(1)).invoke(any())
    }

    @Test
    fun executeSuccessFunctionCallback_errorResult_successFunctionNotInvoked() {
        val mockSuccess: (Any) -> Unit = mock()

        testExecutor.result = null
        testUseCase.execute(mockSuccess)

        verify(mockSuccess, times(0)).invoke(any())
    }

    /**
     * executeBlocking functions
     */

    @Test
    fun executeBlockingCallback_successResult_onSuccessCalled() {
        val callback: UseCaseCallback<Any> = mock()

        // set the result returned by executor
        testExecutor.result = 1000
        testUseCase.executeBlocking(callback)

        verify(callback, times(1)).onSuccess(any())
        verify(callback, times(0)).onError(any())
    }

    @Test
    fun executeBlockingCallback_errorResult_onErrorCalled() {
        val callback: UseCaseCallback<Any> = mock()

        // null will return onError as result
        testExecutor.result = null
        testUseCase.executeBlocking(callback)

        verify(callback, times(0)).onSuccess(any())
        verify(callback, times(1)).onError(any())
    }

    @Test
    fun executeBlockingFunctionCallbacks_successResult_onSuccessCalled() {
        val mockSuccess: (Any) -> Unit = mock()
        val mockError: (Throwable) -> Unit = mock()

        // set the result returned by executor
        testExecutor.result = 1000
        testUseCase.executeBlocking(mockSuccess, mockError)

        verify(mockSuccess, times(1)).invoke(any())
        verify(mockError, times(0)).invoke(any())
    }

    @Test
    fun executeBlockingFunctionCallbacks_errorResult_onErrorCalled() {
        val mockSuccess: (Any) -> Unit = mock()
        val mockError: (Throwable) -> Unit = mock()

        // null will return onError as result
        testExecutor.result = null
        testUseCase.executeBlocking(mockSuccess, mockError)

        verify(mockSuccess, times(0)).invoke(any())
        verify(mockError, times(1)).invoke(any())
    }

    @Test
    fun executeBlockingSuccessFunctionCallback_successResult_onSuccessCalled() {
        val mockSuccess: (Any) -> Unit = mock()

        // set the result returned by executor
        testExecutor.result = 1000
        testUseCase.executeBlocking(mockSuccess)

        verify(mockSuccess, times(1)).invoke(any())
    }

    @Test
    fun executeBlockingSuccessFunctionCallback_errorResult_successFunctionNotInvoked() {
        val mockSuccess: (Any) -> Unit = mock()

        testExecutor.result = null
        testUseCase.executeBlocking(mockSuccess)

        verify(mockSuccess, times(0)).invoke(any())
    }
}