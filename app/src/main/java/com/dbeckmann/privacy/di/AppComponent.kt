package com.dbeckmann.privacy.di

import android.content.Context
import com.dbeckmann.privacy.data.AppScan
import com.dbeckmann.privacy.data.PlayServicesAvailabilityRepository
import com.dbeckmann.privacy.di.annotation.AppScope
import com.dbeckmann.privacy.di.annotation.ObserveScheduler
import com.dbeckmann.privacy.di.usecase.UseCaseModule
import com.dbeckmann.privacy.util.Logger
import dagger.Component
import io.reactivex.Scheduler

@AppScope
@Component(modules = [(AppModule::class), (UseCaseModule::class)])
interface AppComponent {
    val context: Context

    @ObserveScheduler
    fun observeScheduler(): Scheduler

    val logger: Logger
    val appScan: AppScan
    val playServicesAvailability: PlayServicesAvailabilityRepository
}