package com.dbeckmann.privacy.di.annotation

import javax.inject.Scope

/**
 * Created by daniel on 10.12.2017.
 */

@Scope
annotation class AppScope
