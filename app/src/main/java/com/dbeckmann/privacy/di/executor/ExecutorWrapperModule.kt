package com.dbeckmann.privacy.di.executor

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import com.dbeckmann.privacy.di.annotation.ActivityScope
import com.dbeckmann.privacy.di.annotation.ActivityUseCaseExecutor
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import dagger.Module
import dagger.Provides

@Module
class ExecutorWrapperModule {

    @Provides
    @ActivityScope
    fun useCaseExecutorWrapper(viewModel: UseCaseExecutorViewModel): UseCaseExecutor =
        ViewModelUseCaseRxExecutorWrapper(viewModel)

    @Provides
    @ActivityScope
    fun useCaseExecutorViewModel(fragmentActivity: FragmentActivity, @ActivityUseCaseExecutor executor: UseCaseExecutor): UseCaseExecutorViewModel {
        val viewModel = ViewModelProviders.of(fragmentActivity).get(UseCaseExecutorViewModel::class.java)
        viewModel.executor = executor
        return viewModel
    }
}