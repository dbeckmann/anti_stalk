package com.dbeckmann.privacy.di.activity.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dbeckmann.privacy.di.annotation.ViewModelKey
import com.dbeckmann.privacy.view.apps.AllAppsViewModel
import com.dbeckmann.privacy.view.detail.DetailsViewModel
import com.dbeckmann.privacy.view.result.ResultViewModel
import com.dbeckmann.privacy.view.result.tile.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: DaggerViewModelProviderFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ResultViewModel::class)
    abstract fun bindResultViewModel(vm: ResultViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailsViewModel::class)
    abstract fun bindDetailsViewModel(vm: DetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AllAppsViewModel::class)
    abstract fun bindAllAppsViewModel(vm: AllAppsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(vm: MainViewModel): ViewModel
}