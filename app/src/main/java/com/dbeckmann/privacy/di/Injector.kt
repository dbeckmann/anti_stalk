package com.dbeckmann.privacy.di

import androidx.fragment.app.FragmentActivity
import com.dbeckmann.privacy.AppController
import com.dbeckmann.privacy.di.activity.ActivityComponent
import com.dbeckmann.privacy.di.activity.ActivityModule
import com.dbeckmann.privacy.di.activity.DaggerActivityComponent

object Injector {
    private val activityComponentMap: MutableMap<String, ActivityComponent> = mutableMapOf()
    private val lifecycleComponentMap: MutableMap<String, LifecycleAwareComponent> = mutableMapOf()

    @JvmStatic
    fun getApplicationComponent(): AppComponent = AppController.getInstance().appComponent

    @JvmStatic
    fun injectActivityComponent(activity: FragmentActivity): ActivityComponent {
        val component = DaggerActivityComponent.builder()
            .appComponent(getApplicationComponent())
            .activityModule(ActivityModule(activity))
            .build()

        val key = getActivityKey(activity)

        activityComponentMap.forEach {
            println("mapped comp: ${it.key} = ${it.value}")
        }

        activityComponentMap[key] = component
        lifecycleComponentMap[key] = LifecycleAwareComponent(activity)

        return component
    }

    @JvmStatic
    fun getActivityComponent(activity: FragmentActivity): ActivityComponent {
        val key = getActivityKey(activity)
        return getActivityComponent(key)
    }

    @JvmStatic
    fun getActivityComponent(key: String): ActivityComponent {
        if (activityComponentMap.containsKey(key)) {
            return activityComponentMap[key]!!
        } else {
            throw IllegalStateException("could not find ActivityComponent $key")
        }
    }

    @JvmStatic
    fun removeActivityComponent(activity: FragmentActivity) {
        val key = getActivityKey(activity)

        if (activityComponentMap.containsKey(key)) {
            lifecycleComponentMap.remove(key)
            activityComponentMap.remove(key)

            println("ActivityComponent removed for $key")
        }
    }

    fun getActivityKey(activity: FragmentActivity): String {
        return activity::class.java.canonicalName!!
    }
}