package com.dbeckmann.privacy.di

import android.app.Application
import android.content.Context
import android.os.Looper
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import com.dbeckmann.privacy.AppController
import com.dbeckmann.privacy.data.PlayServicesAvailability
import com.dbeckmann.privacy.data.PlayServicesAvailabilityRepository
import com.dbeckmann.privacy.di.annotation.AppScope
import com.dbeckmann.privacy.di.annotation.ObserveScheduler
import com.dbeckmann.privacy.util.DefaultLogger
import com.dbeckmann.privacy.util.Logger
import com.dbeckmann.rxexecutor.RxExecutor
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@Module
class AppModule(
    private val application: AppController
) {
    @Provides
    @AppScope
    fun providesContext(): Context = application.applicationContext

    @Provides
    @AppScope
    fun providesApplication(): Application = application

    @Provides
    @AppScope
    fun providesAppController(): AppController = application

    @Provides
    @AppScope
    @ObserveScheduler
    fun asyncMainThreadScheduler(): Scheduler {
        val asyncMainThreadScheduler = AndroidSchedulers.from(Looper.getMainLooper(), true)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { asyncMainThreadScheduler }

        return asyncMainThreadScheduler
    }

    @Provides
    @AppScope
    fun defaultUseCaseExecutor(@ObserveScheduler observeScheduler: Scheduler)
            : UseCaseExecutor = RxExecutor(Schedulers.io(), observeScheduler, "application")

    @Provides
    @AppScope
    fun defaultLogger(): Logger = DefaultLogger()

    @Provides
    @AppScope
    fun playServicesAvailability(context: Context): PlayServicesAvailabilityRepository = PlayServicesAvailability(context)
}