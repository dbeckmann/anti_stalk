package com.dbeckmann.privacy.di.activity

import androidx.fragment.app.FragmentActivity
import com.dbeckmann.privacy.di.annotation.ActivityScope
import com.dbeckmann.privacy.di.annotation.ActivityUseCaseExecutor
import com.dbeckmann.privacy.di.annotation.ObserveScheduler
import com.dbeckmann.privacy.di.executor.ExecutorWrapperModule
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import com.dbeckmann.rxexecutor.LifecycleAwareRxExecutor
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

@Module(includes = [ExecutorWrapperModule::class])
class ActivityModule(private var fragmentActivity: FragmentActivity) {

    @Provides
    fun providesFragmentActivity(): FragmentActivity = fragmentActivity

    @Provides
    @ActivityUseCaseExecutor
    @ActivityScope
    fun activityUseCaseExecutor(@ObserveScheduler observeScheduler: Scheduler)
            : UseCaseExecutor = LifecycleAwareRxExecutor(Schedulers.io(), observeScheduler, "activity", fragmentActivity.lifecycle)

}