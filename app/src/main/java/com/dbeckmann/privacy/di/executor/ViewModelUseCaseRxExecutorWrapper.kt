package com.dbeckmann.privacy.di.executor

import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import com.dbeckmann.rxexecutor.RxExecutor
import com.dbeckmann.rxexecutor.RxExecutorWrapper
import io.reactivex.Scheduler
import javax.inject.Inject

class ViewModelUseCaseRxExecutorWrapper @Inject constructor(
    private val viewModel: UseCaseExecutorViewModel
) : UseCaseExecutor, RxExecutorWrapper {

    init {
        println("create wrapper: $this")
    }

    override fun <Result> run(executor: Executor<Result>, callback: UseCaseCallback<Result>) {
        println("wrapper $this")
        useCaseExecutor().run(executor, callback)
    }

    override fun <Result> runBlocking(executor: Executor<Result>, callback: UseCaseCallback<Result>) {
        useCaseExecutor().runBlocking(executor, callback)
    }

    override fun stop() = useCaseExecutor().stop()

    override fun isDestroyed(): Boolean = useCaseExecutor().isDestroyed()

    override fun getSubscribeScheduler(): Scheduler = (useCaseExecutor() as RxExecutor).subscribeScheduler

    private fun useCaseExecutor(): UseCaseExecutor {
        return viewModel.executor
    }
}