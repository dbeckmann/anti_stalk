package com.dbeckmann.privacy.di.usecase

import com.dbeckmann.domain.usecase.*
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import com.dbeckmann.privacy.data.AppScan
import com.dbeckmann.privacy.data.DeviceState
import com.dbeckmann.privacy.data.SafetyNetApp
import com.dbeckmann.privacy.di.annotation.SafetyNet
import dagger.Module
import dagger.Provides

@Module
class UseCaseModule {

    @Provides
    fun getInstalledApps(repository: AppScan, useCaseExecutor: UseCaseExecutor) = GetInstalledApps(repository, useCaseExecutor)

    @Provides
    fun getInstalledAppsWithScores(repository: AppScan, useCaseExecutor: UseCaseExecutor) = GetInstalledAppsWithScores(repository, useCaseExecutor)

    @Provides
    fun getSuspiciousApps(repository: AppScan, useCaseExecutor: UseCaseExecutor) = GetSuspiciousApps(repository, useCaseExecutor)

    @Provides
    fun getAppInfoEntityByPackageName(repository: AppScan, useCaseExecutor: UseCaseExecutor) =
        GetAppInfoEntityByPackageName(repository, useCaseExecutor)

    @Provides
    fun getAppInfoEntityByListPackageName(repository: AppScan, useCaseExecutor: UseCaseExecutor) =
        GetAppInfoEntityByListPackageName(repository, useCaseExecutor)

    @Provides
    @SafetyNet
    fun getSafetyNetHarmfulApps(repository: SafetyNetApp, useCaseExecutor: UseCaseExecutor) = GetSuspiciousApps(repository, useCaseExecutor)

    @Provides
    fun getDeviceRootState(repository: DeviceState, useCaseExecutor: UseCaseExecutor) = GetDeviceRootState(repository, useCaseExecutor)

    @Provides
    fun getDeviceAdmins(repository: DeviceState, useCaseExecutor: UseCaseExecutor) = GetDeviceAdmins(repository, useCaseExecutor)

    @Provides
    fun getPhotoAppsWithLocationEnabled(repository: DeviceState, useCaseExecutor: UseCaseExecutor) =
        GetPhotoAppsWithLocationEnabled(repository, useCaseExecutor)

    @Provides
    fun hasSafetyNetEnabled(repository: DeviceState, useCaseExecutor: UseCaseExecutor) = HasSafetyNetEnabled(repository, useCaseExecutor)

    @Provides
    fun hasUsbDebuggingEnabled(repository: DeviceState, useCaseExecutor: UseCaseExecutor) = HasUsbDebuggingEnabled(repository, useCaseExecutor)

    @Provides
    fun hasKeyguardSecured(repository: DeviceState, useCaseExecutor: UseCaseExecutor) = HasKeyguardSecured(repository, useCaseExecutor)
}