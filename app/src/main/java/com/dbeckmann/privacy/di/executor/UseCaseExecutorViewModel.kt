package com.dbeckmann.privacy.di.executor

import androidx.lifecycle.ViewModel
import com.dbeckmann.interactor.usecase.UseCaseExecutor

class UseCaseExecutorViewModel : ViewModel() {
    lateinit var executor: UseCaseExecutor
}