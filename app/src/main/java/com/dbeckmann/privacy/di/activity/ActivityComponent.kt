package com.dbeckmann.privacy.di.activity

import com.dbeckmann.privacy.di.AppComponent
import com.dbeckmann.privacy.di.activity.viewmodel.ViewModelModule
import com.dbeckmann.privacy.di.annotation.ActivityScope
import com.dbeckmann.privacy.di.annotation.ActivityUseCaseExecutor
import com.dbeckmann.privacy.di.usecase.UseCaseModule
import com.dbeckmann.privacy.view.main.MainActivity
import com.dbeckmann.interactor.usecase.UseCaseExecutor
import com.dbeckmann.privacy.view.apps.AllAppsFragment
import com.dbeckmann.privacy.view.detail.DetailsActivity
import com.dbeckmann.privacy.view.result.ResultFragment
import dagger.Component

@ActivityScope
@Component(modules = [(ActivityModule::class), (UseCaseModule::class), (ViewModelModule::class)], dependencies = [(AppComponent::class)])
interface ActivityComponent {

    @ActivityUseCaseExecutor
    fun activityExecutor() : UseCaseExecutor

    fun inject(activity: MainActivity)
    fun inject(activity: DetailsActivity)
    fun inject(fragment: ResultFragment)
    fun inject(fragment: AllAppsFragment)
}
