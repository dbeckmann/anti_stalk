package com.dbeckmann.privacy

import android.app.Application
import com.dbeckmann.privacy.di.AppComponent
import com.dbeckmann.privacy.di.AppModule
import com.dbeckmann.privacy.di.DaggerAppComponent
import com.dbeckmann.privacy.util.DefaultLogger
import io.reactivex.plugins.RxJavaPlugins

class AppController : Application() {

    companion object {
        const val TAG = "AppController"
        private var instance: AppController? = null

        fun getInstance(): AppController = instance ?: throw IllegalStateException("there should be an instance of application")
    }

    val appComponent: AppComponent
    private val appModule: AppModule = AppModule(this)

    init {
        appComponent = DaggerAppComponent.builder()
            .appModule(appModule)
            .build()

        instance = this
    }

    private val logger = DefaultLogger()

    override fun onCreate() {
        super.onCreate()

        RxJavaPlugins.setErrorHandler { throwable: Throwable ->
            logger.e(TAG, "undelivered error: $throwable")
            throwable.printStackTrace()
        }
    }
}