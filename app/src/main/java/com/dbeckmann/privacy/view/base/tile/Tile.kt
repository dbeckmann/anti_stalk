package com.dbeckmann.privacy.view.base.tile

import android.view.ViewGroup
import com.dbeckmann.interactor.usecase.UseCaseCallback

/**
 * Created by daniel on 05.05.2018.
 */
interface Tile<out Result> {
    fun getItemViewType(): Int
    fun getData(callback: UseCaseCallback<Result>)
    fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder

    /**
     * default behavior just check supertype here
     */
    fun tilesAreSame(other: Tile<Any>?): Boolean {
        return this.javaClass == other?.javaClass
    }
}