package com.dbeckmann.privacy.view.result.tile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.view.base.tile.AbstractTileViewHolder
import com.dbeckmann.privacy.view.base.tile.Tile

class CenteredTextTile(val stringId: Int) : Tile<Int> {
    override fun getItemViewType(): Int = R.id.centeredTextTile

    override fun getData(callback: UseCaseCallback<Int>) {
        callback.onSuccess(stringId)
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_centered_text_tile, parent, false)
        return CenteredTextTileViewHolder(view)
    }
}

class CenteredTextTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private val text: TextView = itemView.findViewById(R.id.tv_text)

    override fun setTile(tile: Tile<Any>) {
        tile.getData(
            UseCaseCallback {
                val result = it as Int
                render(itemView.context.getString(result))
            }
        )
    }

    private fun render(text: String) {
        this.text.text = text
    }
}