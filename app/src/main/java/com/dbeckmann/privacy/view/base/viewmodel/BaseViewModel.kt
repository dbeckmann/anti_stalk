package com.dbeckmann.privacy.view.base.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {

    private val _state: MutableLiveData<BaseState> = MutableLiveData()
    val state: LiveData<BaseState> = _state

    fun setState(state: BaseState) {
        _state.postValue(state)
    }
}