package com.dbeckmann.privacy.view.detail.tile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.view.base.tile.AbstractTileViewHolder
import com.dbeckmann.privacy.view.base.tile.Tile
import com.dbeckmann.privacy.view.detail.PropertyMap

class PropertyTile(private val property: String) : Tile<String> {
    override fun getItemViewType(): Int = R.id.propertyTile

    override fun getData(callback: UseCaseCallback<String>) {
        callback.onSuccess(property)
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_property, parent, false)
        return PropertyTileViewHolder(view)
    }
}

class PropertyTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private val icon: ImageView = itemView.findViewById(R.id.iv_permission_icon)
    private val label: TextView = itemView.findViewById(R.id.tv_permission_label)
    private val description: TextView = itemView.findViewById(R.id.tv_permission_description)

    private val pm = itemView.context.packageManager

    override fun setTile(tile: Tile<Any>) {
        tile.getData(
            UseCaseCallback {
                val result = it as String
                render(result)
            }
        )
    }

    private fun render(property: String) {
        try {
            val propertyDetail = PropertyMap.map[property]

            propertyDetail?.also {
                icon.setImageDrawable(itemView.context.getDrawable(it.drawableId))
                label.text = itemView.context.getText(it.nameId)
                description.text = itemView.context.getText(it.descriptionId)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}