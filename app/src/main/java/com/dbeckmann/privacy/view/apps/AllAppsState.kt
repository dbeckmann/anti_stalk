package com.dbeckmann.privacy.view.apps

import com.dbeckmann.privacy.view.base.viewmodel.BaseState

sealed class AllAppsState : BaseState
object UpdatedTiles : AllAppsState()