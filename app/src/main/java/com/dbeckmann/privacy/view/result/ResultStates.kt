package com.dbeckmann.privacy.view.result

import com.dbeckmann.privacy.view.base.viewmodel.BaseState

sealed class ResultState : BaseState
object UpdatedTiles : ResultState()
object ButtonViewResults : ResultState()
data class ButtonUninstallClicked(val packageName: String) : ResultState()