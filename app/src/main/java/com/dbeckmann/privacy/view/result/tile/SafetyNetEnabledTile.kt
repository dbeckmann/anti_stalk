package com.dbeckmann.privacy.view.result.tile

import android.content.Intent
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.util.setGone
import com.dbeckmann.privacy.util.setVisible
import com.dbeckmann.privacy.view.base.compound.ResultInfoView
import com.dbeckmann.privacy.view.base.tile.AbstractTileViewHolder
import com.dbeckmann.privacy.view.base.tile.Tile

class SafetyNetEnabledTile(private val isEnabled: Boolean) : Tile<Boolean> {
    override fun getItemViewType(): Int = R.id.safetyNetEnabledTile

    override fun getData(callback: UseCaseCallback<Boolean>) {
        callback.onSuccess(isEnabled)
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_safety_net_enabled, parent, false)
        return SafetyNetEnabledViewHolder(view)
    }
}

class SafetyNetEnabledViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private val resultView: ResultInfoView = itemView.findViewById(R.id.riv_safety_net_enabled)
    private val buttonEnableGoogleProtect: Button = itemView.findViewById(R.id.b_enable_google_protect)

    override fun setTile(tile: Tile<Any>) {
        tile.getData(UseCaseCallback
        {
            render(it as Boolean)
        })
    }

    private fun render(isEnabled: Boolean) {
        when (isEnabled) {
            true -> {
                resultView.setTitleText(R.string.main_safety_net_enabled_success_title)
                resultView.setInfoText(R.string.main_safety_net_enabled_success_text)
                resultView.setIcon(R.drawable.drawable_check)
                buttonEnableGoogleProtect.setGone()
            }
            false -> {
                resultView.setTitleText(R.string.main_safety_net_enabled_failed_title)
                resultView.setInfoText(R.string.main_safety_net_enabled_failed_text)
                resultView.setIcon(R.drawable.ic_error_outline_orange_24dp)
                buttonEnableGoogleProtect.setVisible()
            }
        }

        buttonEnableGoogleProtect.setOnClickListener {
            val intent = Intent(Settings.ACTION_SECURITY_SETTINGS)
            itemView.context.startActivity(intent)
        }
    }
}