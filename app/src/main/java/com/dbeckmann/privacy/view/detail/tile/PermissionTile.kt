package com.dbeckmann.privacy.view.detail.tile

import android.content.pm.PackageItemInfo
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dbeckmann.domain.model.PermissionEntity
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.util.setGone
import com.dbeckmann.privacy.util.setVisible
import com.dbeckmann.privacy.view.base.tile.AbstractTileViewHolder
import com.dbeckmann.privacy.view.base.tile.Tile
import com.dbeckmann.privacy.view.detail.PermissionMap

class PermissionTile(private val permission: PermissionEntity) : Tile<PermissionEntity> {
    override fun getItemViewType(): Int = R.id.permissionTile

    override fun getData(callback: UseCaseCallback<PermissionEntity>) {
        callback.onSuccess(permission)
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_permission, parent, false)
        return PermissionTileViewHolder(view)
    }
}

class PermissionTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private val icon: ImageView = itemView.findViewById(R.id.iv_permission_icon)
    private val label: TextView = itemView.findViewById(R.id.tv_permission_label)
    private val description: TextView = itemView.findViewById(R.id.tv_permission_description)
    private val isGranted: View = itemView.findViewById(R.id.tv_is_granted)
    private val isNotGranted: View = itemView.findViewById(R.id.tv_is_not_granted)

    private val pm = itemView.context.packageManager

    override fun setTile(tile: Tile<Any>) {
        tile.getData(
            UseCaseCallback {
                val result = it as PermissionEntity
                render(result)
            }
        )
    }

    private fun render(permission: PermissionEntity) {
        try {
            val permissionInfo = pm.getPermissionInfo(permission.name, PackageManager.GET_META_DATA)

            icon.setImageDrawable(getPermissionDrawable(permission.name))
            label.text = permissionInfo.loadLabel(pm)
            description.text = permissionInfo.loadDescription(pm) ?: itemView.context.getText(R.string.details_no_detailed_description)

            if (permission.isGranted) {
                isGranted.setVisible()
                isNotGranted.setGone()
            } else {
                isGranted.setGone()
                isNotGranted.setVisible()
            }

            if (!permission.isDangerous) {
                isGranted.setGone()
                isNotGranted.setGone()
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getPermissionDrawable(permission: String): Drawable? {
        val drawableByPermissionInfo = getDrawableByPermissionInfo(permission)
        val drawableByGroupInfo = getDrawableByPermissionGroupInfo(permission)

        return when {
            drawableByPermissionInfo != null -> drawableByPermissionInfo
            drawableByGroupInfo != null -> drawableByGroupInfo
            else -> itemView.context.getDrawable(R.drawable.ic_perm_device_information_black_24dp)!!
        }
    }

    private fun getDrawableByPermissionInfo(permission: String): Drawable? {
        var drawable: Drawable? = null

        try {
            val permissionInfo = pm.getPermissionInfo(permission, 0)
            drawable = loadItemInfoIcon(permissionInfo)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        return drawable
    }

    private fun getDrawableByPermissionGroupInfo(permission: String): Drawable? {
        var drawable: Drawable? = null

        try {
            val permissionInfo = pm.getPermissionInfo(permission, 0)
            val groupInfo = pm.getPermissionGroupInfo(PermissionMap.PLATFORM_PERMISSIONS[permissionInfo.name], 0)
            drawable = loadItemInfoIcon(groupInfo)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        return drawable
    }

    private fun loadItemInfoIcon(itemInfo: PackageItemInfo): Drawable? {
        var icon: Drawable? = null
        if (itemInfo.icon > 0) {
            icon = pm.getResourcesForApplication(itemInfo.packageName).getDrawable(itemInfo.icon, null)
        }

        return icon
    }

}