package com.dbeckmann.privacy.view.detail.tile

import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.privacy.BuildConfig
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.view.base.tile.AbstractTileViewHolder
import com.dbeckmann.privacy.view.base.tile.Tile
import java.text.SimpleDateFormat
import java.util.*

class AppInfoTile(private val appInfo: AppInfoEntity) : Tile<AppInfoEntity> {
    override fun getItemViewType(): Int = R.id.appInfoTile

    override fun getData(callback: UseCaseCallback<AppInfoEntity>) {
        callback.onSuccess(appInfo)
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_app_info, parent, false)
        return AppInfoTileViewHolder(view)
    }
}

class AppInfoTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private val icon: ImageView = itemView.findViewById(R.id.iv_app_icon)
    private val packageName: TextView = itemView.findViewById(R.id.tv_package_name)
    private val appName: TextView = itemView.findViewById(R.id.tv_app_name)
    private val versionName: TextView = itemView.findViewById(R.id.tv_version_name)
    private val installTime: TextView = itemView.findViewById(R.id.tv_install_time)

    override fun setTile(tile: Tile<Any>) {
        tile.getData(
            UseCaseCallback {
                val result = it as AppInfoEntity
                render(result)
            }
        )
    }

    private fun render(appInfo: AppInfoEntity) {
        val iconDrawable = itemView.context.packageManager.getApplicationIcon(appInfo.packageName)

        icon.setImageDrawable(iconDrawable)
        versionName.text = appInfo.versionName
        packageName.text = appInfo.packageName


        if (BuildConfig.DEBUG) {
            appName.text = "${appInfo.appName} (c: ${appInfo.confidenceScore}) (s: ${appInfo.score})"
        } else {
            appName.text = appInfo.appName
        }

        val dateFormat = SimpleDateFormat("dd.MM.yyyy", itemView.context.resources.configuration.locale)
        installTime.text =
            Html.fromHtml(itemView.context.getString(R.string.details_app_info_install_time, dateFormat.format(Date(appInfo.firstInstallTime))))
    }
}