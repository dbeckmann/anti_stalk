package com.dbeckmann.privacy.view.base.tile

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

open class BaseTileRecyclerViewAdapter(
    private val presenter: BaseTilePresenter
) : RecyclerView.Adapter<AbstractTileViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractTileViewHolder =
        presenter.getTileByItemViewType(viewType).createViewHolder(parent)

    override fun getItemCount(): Int = presenter.getItemCount()
    override fun onBindViewHolder(holder: AbstractTileViewHolder, position: Int) = presenter.onBindViewHolder(holder, position)
    override fun getItemViewType(position: Int): Int = presenter.getItemViewType(position)
}