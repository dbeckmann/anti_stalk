package com.dbeckmann.privacy.view.result

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.di.Injector
import com.dbeckmann.privacy.di.activity.viewmodel.DaggerViewModelProviderFactory
import com.dbeckmann.privacy.view.base.TileDelegate
import com.dbeckmann.privacy.view.base.tile.BaseTileRecyclerViewAdapter
import com.dbeckmann.privacy.view.result.tile.MainViewModel
import javax.inject.Inject

class ResultFragment : Fragment() {

    @Inject lateinit var viewModelProvider: DaggerViewModelProviderFactory

    private lateinit var resultViewModel: ResultViewModel
    private lateinit var mainViewModel: MainViewModel

    private lateinit var rvAdapter: BaseTileRecyclerViewAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var recyclerView: RecyclerView
    private lateinit var tileDelegate: TileDelegate

    private var currentUninstallPackageName: String = ""

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.also {
            Injector.injectActivityComponent(it).inject(this)

            resultViewModel = ViewModelProvider(this, viewModelProvider).get(ResultViewModel::class.java)
            mainViewModel = ViewModelProvider(it, viewModelProvider).get(MainViewModel::class.java)

            tileDelegate = TileDelegate(resultViewModel)

            layoutManager = LinearLayoutManager(context)
            rvAdapter = BaseTileRecyclerViewAdapter(tileDelegate)
            recyclerView.adapter = rvAdapter
            recyclerView.layoutManager = layoutManager

            // observe state for this fragments viewmodel
            resultViewModel.state.observe(viewLifecycleOwner, Observer { state -> processState(state as ResultState) })

            // observe new results in mainViewModel
            mainViewModel.result.observe(viewLifecycleOwner, Observer { result -> resultViewModel.updateResults(result) })
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_main, container, false)

        recyclerView = v.findViewById(R.id.rv_list)

        return v
    }

    private fun processState(state: ResultState) {
        when (state) {
            is UpdatedTiles -> tileDelegate.updateList(rvAdapter)
            is ButtonUninstallClicked -> requestUninstall(state.packageName)
            is ButtonViewResults -> forwardToAllResults()
        }
    }

    private fun requestUninstall(packageName: String) {
        currentUninstallPackageName = packageName

        val intent = Intent().apply {
            action = Intent.ACTION_DELETE
            data = Uri.parse("package:${packageName}")
            putExtra(Intent.EXTRA_RETURN_RESULT, true)
        }

        startActivityForResult(intent, UNINSTALL_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == UNINSTALL_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            mainViewModel.appUninstalled(currentUninstallPackageName)
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun forwardToAllResults() {
        mainViewModel.showAllResults()
    }

    companion object {
        const val UNINSTALL_REQUEST_CODE = 100
    }
}