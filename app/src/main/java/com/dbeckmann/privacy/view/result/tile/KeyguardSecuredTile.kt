package com.dbeckmann.privacy.view.result.tile

import android.content.Intent
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.util.setGone
import com.dbeckmann.privacy.util.setVisible
import com.dbeckmann.privacy.view.base.compound.ResultInfoView
import com.dbeckmann.privacy.view.base.tile.AbstractTileViewHolder
import com.dbeckmann.privacy.view.base.tile.Tile

class KeyguardSecuredTile(private val isSecure: Boolean) : Tile<Boolean> {
    override fun getItemViewType(): Int = R.id.keyguardSecuredTile

    override fun getData(callback: UseCaseCallback<Boolean>) {
        callback.onSuccess(isSecure)
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_keyguard_secure, parent, false)
        return KeyguardSecuredTileViewHolder(view)
    }
}

class KeyguardSecuredTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private val resultView: ResultInfoView = itemView.findViewById(R.id.riv_keyguard)
    private val buttonSetKeyguard: Button = itemView.findViewById(R.id.b_set_keyguard)

    override fun setTile(tile: Tile<Any>) {
        tile.getData(UseCaseCallback {
            render(it as Boolean)
        })
    }

    private fun render(isSecure: Boolean) {
        when (isSecure) {
            true -> {
                resultView.setTitleText(R.string.main_keyguard_secure_success_title)
                resultView.setInfoText(R.string.main_keyguard_secure_success_text)
                resultView.setIcon(R.drawable.drawable_check)
                buttonSetKeyguard.setGone()
            }
            false -> {
                resultView.setTitleText(R.string.main_keyguard_secure_failed_title)
                resultView.setInfoText(R.string.main_keyguard_secure_failed_text)
                resultView.setIcon(R.drawable.ic_error_outline_orange_24dp)
                buttonSetKeyguard.setVisible()
            }
        }

        buttonSetKeyguard.setOnClickListener {
            val intent = Intent(Settings.ACTION_SECURITY_SETTINGS)
            itemView.context.startActivity(intent)
        }

    }
}