package com.dbeckmann.privacy.view.base.compound

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.dbeckmann.privacy.R

class ProgressView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attributeSet, defStyleAttr) {

    private val infoText: TextView

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_processing, this, true)

        infoText = this.findViewById(R.id.tv_info_text)
    }

    fun setInfoText(@StringRes stringId: Int) {
        infoText.setText(stringId)
    }

    fun setInfoText(text: String) {
        infoText.setText(text)
    }
}