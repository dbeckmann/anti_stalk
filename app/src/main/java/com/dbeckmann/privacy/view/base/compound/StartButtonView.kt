package com.dbeckmann.privacy.view.base.compound

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.dbeckmann.privacy.R

class StartButtonView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attributeSet, defStyleAttr) {

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_start_analysis, this, true)
        initView(attributeSet)
    }

    private fun initView(attributeSet: AttributeSet?) {

    }
}