package com.dbeckmann.privacy.view.apps

import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.usecase.GetAppInfoEntityByListPackageName
import com.dbeckmann.privacy.data.DeviceScanResult
import com.dbeckmann.privacy.util.Logger
import com.dbeckmann.privacy.view.apps.tile.AppTile
import com.dbeckmann.privacy.view.base.tile.Tile
import com.dbeckmann.privacy.view.base.tile.TileDataHolder
import com.dbeckmann.privacy.view.base.viewmodel.BaseViewModel
import javax.inject.Inject

class AllAppsViewModel @Inject constructor(
    private val logger: Logger,
    private val getAppInfoEntityByListPackageName: GetAppInfoEntityByListPackageName
) : BaseViewModel(), TileDataHolder {

    override val tiles: MutableList<Tile<Any>> = mutableListOf()

    fun updateResults(result: DeviceScanResult) {
        logger.d(TAG, "updateResult")
        buildTiles(result.installedApps)
    }

    fun updateResults(packageNames: List<String>) {
        getAppInfoEntityByListPackageName.execute(packageNames) {
            buildTiles(it)
        }
    }

    private fun buildTiles(appInfos: List<AppInfoEntity>) {
        logger.d(TAG, "buildTiles")

        tiles.clear()

        appInfos
            .distinct()
            .filter { !it.hasSystemAppFlag }
            .sortedByDescending { it.confidenceScore }
            .forEach {
                tiles.add(AppTile(it))
            }

        setState(UpdatedTiles)
    }

    companion object {
        const val TAG = "AllAppsViewModel"
    }
}