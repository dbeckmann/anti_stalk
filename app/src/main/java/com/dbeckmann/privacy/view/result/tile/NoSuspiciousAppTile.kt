package com.dbeckmann.privacy.view.result.tile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.view.base.tile.AbstractTileViewHolder
import com.dbeckmann.privacy.view.base.tile.Tile

class NoSuspiciousAppTile(
    private val numApps: Int,
    private val buttonViewResultsClick: () -> Unit
) : Tile<Int> {
    override fun getItemViewType(): Int = R.id.noSuspiciousAppTile

    override fun getData(callback: UseCaseCallback<Int>) {
        callback.onSuccess(numApps)
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_nothing_found_tile, parent, false)
        return NoSuspiciousAppTileViewHolder(view, buttonViewResultsClick)
    }
}

class NoSuspiciousAppTileViewHolder(
    itemView: View,
    val buttonViewResultsClick: () -> Unit
) : AbstractTileViewHolder(itemView) {

    private val info: TextView = itemView.findViewById(R.id.tv_description)
    private val buttonViewResults: Button = itemView.findViewById(R.id.b_details)

    override fun setTile(tile: Tile<Any>) {
        tile.getData(
            UseCaseCallback {
                render(it as Int)
            }
        )
    }

    private fun render(num: Int) {
        info.text = String.format(itemView.context.getText(R.string.main_no_suspicious_apps_found_text).toString(), num)

        buttonViewResults.setOnClickListener {
            buttonViewResultsClick()
        }
    }
}