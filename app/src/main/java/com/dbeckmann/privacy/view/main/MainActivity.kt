package com.dbeckmann.privacy.view.main

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.di.Injector
import com.dbeckmann.privacy.di.activity.viewmodel.DaggerViewModelProviderFactory
import com.dbeckmann.privacy.util.setGone
import com.dbeckmann.privacy.util.setInvisible
import com.dbeckmann.privacy.util.setVisible
import com.dbeckmann.privacy.view.apps.AllAppsFragment
import com.dbeckmann.privacy.view.base.compound.ProgressView
import com.dbeckmann.privacy.view.result.ResultFragment
import com.dbeckmann.privacy.view.result.tile.MainViewModel
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.tabs.TabLayout
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject lateinit var viewModelProvider: DaggerViewModelProviderFactory

    private lateinit var mainViewModel: MainViewModel

    private lateinit var toolbar: Toolbar
    private lateinit var viewPager: ViewPager
    private lateinit var pagerAdapter: Adapter
    private lateinit var tabLayout: TabLayout
    private lateinit var progressView: ProgressView
    private lateinit var appbarLayout: AppBarLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Injector.injectActivityComponent(this).inject(this)

        mainViewModel = ViewModelProvider(this, viewModelProvider).get(MainViewModel::class.java)
        mainViewModel.state.observe(this, Observer { state -> processState(state as MainState) })

        toolbar = findViewById(R.id.toolbar)
        viewPager = findViewById(R.id.viewpager)
        tabLayout = findViewById(R.id.tab_layout)
        progressView = findViewById(R.id.v_progress_view)
        appbarLayout = findViewById(R.id.appbar_layout)

        initViewPager()
        initTabs()

        setSupportActionBar(toolbar)
        toolbar.setTitle(R.string.app_name)

        mainViewModel.doDeviceAnalysis()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean =
        when (item.itemId) {
            R.id.restart_scan -> {
                mainViewModel.doDeviceAnalysis()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    private fun initViewPager() {
        pagerAdapter = Adapter(supportFragmentManager, this).apply {
            setPages(listOf(PagerView.RESULT, PagerView.ALL_APPS))
        }

        viewPager.adapter = pagerAdapter
    }

    private fun initTabs() {
        tabLayout.setupWithViewPager(viewPager)
    }

    private fun processState(state: MainState) {
        when (state) {
            is ProcessingStarted -> showLoading()
            is ProcessingFinished -> hideLoading()
            is ShowAllResults -> showInstalledApps()
            is UpdateLoadingText -> {
                showLoading()
                setLoadingText(state.stringId)
            }
        }
    }

    private fun showInstalledApps() {
        viewPager.setCurrentItem(1, true)
    }

    private fun showLoading() {
        progressView.setVisible()
        appbarLayout.setInvisible()
    }

    private fun hideLoading() {
        progressView.setGone()
        appbarLayout.setVisible()
    }

    private fun setLoadingText(@StringRes stringId: Int) = progressView.setInfoText(stringId)

    class Adapter(fm: FragmentManager, val context: Context) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
        private val pages: MutableList<PagerView> = mutableListOf()

        fun setPages(pages: List<PagerView>) {
            this.pages.apply {
                clear()
                addAll(pages)
                notifyDataSetChanged()
            }
        }

        override fun getItem(position: Int): Fragment = pages[position].fragment
        override fun getPageTitle(position: Int): CharSequence? = context.getString(pages[position].titleId)
        override fun getCount(): Int = pages.size
    }

    enum class PagerView(
        val titleId: Int,
        val fragment: Fragment
    ) {
        RESULT(R.string.tab_result, ResultFragment()),
        ALL_APPS(R.string.tab_all_apps, AllAppsFragment())
    }
}
