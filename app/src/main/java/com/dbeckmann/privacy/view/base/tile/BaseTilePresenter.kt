package com.dbeckmann.privacy.view.base.tile

interface BaseTilePresenter {
    fun getItemCount() : Int
    fun onBindViewHolder(tileItem: TileItem, pos: Int)
    fun getItemViewType(pos: Int) : Int
    fun getTileByItemViewType(itemViewType: Int) : Tile<Any>
}