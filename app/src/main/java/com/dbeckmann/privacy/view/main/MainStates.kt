package com.dbeckmann.privacy.view.main

import com.dbeckmann.privacy.view.base.viewmodel.BaseState

sealed class MainState : BaseState
object ProcessingStarted : MainState()
object ProcessingFinished : MainState()
object ShowAllResults : MainState()
data class UpdateLoadingText(val stringId: Int) : MainState()