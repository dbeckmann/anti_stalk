package com.dbeckmann.privacy.view.result.tile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.view.base.tile.AbstractTileViewHolder
import com.dbeckmann.privacy.view.base.tile.Tile
import com.dbeckmann.interactor.usecase.UseCaseCallback

class HeaderTile(val stringId: Int) : Tile<Int> {
    override fun getItemViewType(): Int = R.id.headerTile

    override fun getData(callback: UseCaseCallback<Int>) {
        callback.onSuccess(stringId)
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_header_tile, parent, false)
        return HeaderTileViewHolder(view)
    }
}

class HeaderTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private val title: TextView = itemView.findViewById(R.id.tv_title)

    override fun setTile(tile: Tile<Any>) {
        tile.getData(
            UseCaseCallback(
                {
                    val result = it as Int
                    render(itemView.context.getString(result))
                },
                {
                    // do nothing
                })
        )
    }

    private fun render(title: String) {
        this.title?.text = title
    }
}