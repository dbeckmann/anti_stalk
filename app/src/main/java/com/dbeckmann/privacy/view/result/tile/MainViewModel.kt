package com.dbeckmann.privacy.view.result.tile

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.model.RootStateEntity
import com.dbeckmann.domain.usecase.*
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.data.DeviceScanResult
import com.dbeckmann.privacy.data.SafetyNetNoPlayServicesException
import com.dbeckmann.privacy.di.annotation.SafetyNet
import com.dbeckmann.privacy.util.Logger
import com.dbeckmann.privacy.view.base.viewmodel.BaseViewModel
import com.dbeckmann.privacy.view.main.ProcessingFinished
import com.dbeckmann.privacy.view.main.ProcessingStarted
import com.dbeckmann.privacy.view.main.ShowAllResults
import com.dbeckmann.privacy.view.main.UpdateLoadingText
import com.dbeckmann.rxexecutor.toSingle
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val logger: Logger,
    private val getInstalledApps: GetInstalledAppsWithScores,
    private val getSuspiciousApps: GetSuspiciousApps,
    @SafetyNet private val getSafetyNetApps: GetSuspiciousApps,
    private val getDeviceRootState: GetDeviceRootState,
    private val hasSafetyNetEnabled: HasSafetyNetEnabled,
    private val hasKeyguardSecured: HasKeyguardSecured,
    private val getPhotoAppsWithLocationEnabled: GetPhotoAppsWithLocationEnabled
) : BaseViewModel() {

    val result: MutableLiveData<DeviceScanResult> = MutableLiveData()

    private var allApps = mutableListOf<AppInfoEntity>()
    private val suspiciousApps = mutableListOf<AppInfoEntity>()
    private var locationEnabledPhotoApps = mutableListOf<AppInfoEntity>()
    private var attestationResult: RootStateEntity? = null
    private var safetyNetEnabled: Boolean = false
    private var keyguardSecured: Boolean = false

    @SuppressLint("CheckResult")
    fun doDeviceAnalysis() {
        logger.d(TAG, "doDeviceAnalysis")

        resetState()

        setState(ProcessingStarted)
        setState(UpdateLoadingText(R.string.main_loading_info_text_processing))

        suspiciousApps.clear()

        Single.just(false)
            .flatMap { getInstalledApps.toSingle().doOnSuccess { allApps.addAll(it) } }
            .flatMap { getSuspiciousApps.toSingle().doOnSuccess { addToSuspiciousApps(it) } }
            .flatMap { hasKeyguardSecured.toSingle().doOnSuccess { keyguardSecured = it } }
            .flatMap { getPhotoAppsWithLocationEnabled.toSingle().doOnSuccess { locationEnabledPhotoApps.addAll(it) } }
            .flatMap {
                getSafetyNetApps.toSingle()
                    .doOnSuccess { addToSuspiciousApps(it) }
                    .onErrorReturn { listOf() }
            }
            .flatMap {
                hasSafetyNetEnabled.toSingle()
                    .doOnSuccess { safetyNetEnabled = it }
                    .onErrorReturn { false }
            }
            .flatMap {
                setState(UpdateLoadingText(R.string.main_loading_info_text_safetynet_attestation))
                getDeviceRootState.toSingle()
                    .timeout(TIMEOUT_SAFETYNET_ATTESTATION, TimeUnit.SECONDS)
                    .doOnSuccess { attestationResult = it }
                    .onErrorReturn {
                        val rootState = when (it) {
                            is SafetyNetNoPlayServicesException -> RootStateEntity(RootStateEntity.Result.FAILED_PLAY_SERVICES)
                            else -> RootStateEntity(RootStateEntity.Result.FAILED_API)
                        }

                        attestationResult = rootState
                        rootState
                    }
            }
            .subscribe(
                {
                    createDeviceScanResult()
                    setState(ProcessingFinished)
                },
                {
                    it.printStackTrace()
                })
    }

    private fun resetState() {
        allApps.clear()
        suspiciousApps.clear()
        locationEnabledPhotoApps.clear()
        attestationResult = null
        safetyNetEnabled = false
        keyguardSecured = false
    }

    fun appUninstalled(packageName: String) = removePackageFromDeviceScanResult(packageName)
    fun showAllResults() = setState(ShowAllResults)

    private fun addToSuspiciousApps(apps: List<AppInfoEntity>) {
        suspiciousApps.addAll(apps)
    }

    private fun createDeviceScanResult() {
        val scanResult = DeviceScanResult(
            installedApps = allApps,
            suspiciousApps = suspiciousApps,
            locationEnabledPhotoApps = locationEnabledPhotoApps,
            attestationResult = attestationResult!!,
            safetyNetEnabled = safetyNetEnabled,
            hasKeyguardSecured = keyguardSecured
        )

        result.postValue(scanResult)
    }

    private fun removePackageFromDeviceScanResult(packageName: String) {
        allApps.removePackage(packageName)
        suspiciousApps.removePackage(packageName)
        locationEnabledPhotoApps.removePackage(packageName)

        createDeviceScanResult()
    }

    private fun MutableList<AppInfoEntity>.removePackage(packageName: String) {
        this.find { it.packageName == packageName }?.also {
            removeAt(this.indexOfFirst { it.packageName == packageName })
        }
    }

    companion object {
        const val TAG = "MainViewModel"
        const val TIMEOUT_SAFETYNET_ATTESTATION = 30L
    }
}