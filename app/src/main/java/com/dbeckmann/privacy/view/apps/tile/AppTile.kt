package com.dbeckmann.privacy.view.apps.tile

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.data.AppScan
import com.dbeckmann.privacy.util.setGone
import com.dbeckmann.privacy.util.setVisible
import com.dbeckmann.privacy.view.base.tile.AbstractTileViewHolder
import com.dbeckmann.privacy.view.base.tile.Tile
import com.dbeckmann.privacy.view.detail.DetailsActivity

open class AppTile(val appInfo: AppInfoEntity) : Tile<AppInfoEntity> {
    override fun getItemViewType(): Int = R.id.appTile

    override fun getData(callback: UseCaseCallback<AppInfoEntity>) {
        callback.onSuccess(appInfo)
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_app, parent, false)
        return AppTileViewHolder(view)
    }

    override fun tilesAreSame(other: Tile<Any>?): Boolean {
        return when (other) {
            is AppTile -> {
                other.appInfo.packageName == appInfo.packageName
            }
            else -> false
        }
    }
}

open class AppTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    protected var container: View? = null
    protected var icon: ImageView? = null
    protected var appName: TextView? = null
    protected var properties: TextView? = null
    protected var buttonDetails: Button? = null
    protected var dangerStripe: View? = null

    init {
        container = itemView.findViewById(R.id.cl_container)
        icon = itemView.findViewById(R.id.iv_app_icon)
        appName = itemView.findViewById(R.id.tv_app_name)
        properties = itemView.findViewById(R.id.tv_properties)
        buttonDetails = itemView.findViewById(R.id.b_details)
        dangerStripe = itemView.findViewById(R.id.v_danger_stripe)
    }

    override fun setTile(tile: Tile<Any>) {
        tile.getData(
            UseCaseCallback {
                val result = it as AppInfoEntity
                render(result)
            }
        )
    }

    open fun render(data: AppInfoEntity) {
        val iconDrawable = itemView.context.packageManager.getApplicationIcon(data.packageName)

        icon?.setImageDrawable(iconDrawable)
        appName?.text = data.appName
        properties?.text = buildPropertiesString(data)

        setDangerStripe(data)

        container?.setOnClickListener {
            forwardToDetailsView(data.packageName)
        }

        buttonDetails?.setOnClickListener {
            forwardToDetailsView(data.packageName)
        }
    }

    private fun setDangerStripe(appInfo: AppInfoEntity) {
        when {
            appInfo.confidenceScore >= AppScan.CONFIDENCE_SCORE_THRESHOLD -> {
                dangerStripe?.apply {
                    setVisible()
                    setBackgroundColor(resources.getColor(R.color.danger))
                }
            }
            appInfo.confidenceScore > 0.5F && appInfo.confidenceScore < AppScan.CONFIDENCE_SCORE_THRESHOLD -> {
                dangerStripe?.apply {
                    setVisible()
                    setBackgroundColor(resources.getColor(R.color.warning))
                }
            }
            else -> dangerStripe?.setGone()
        }
    }

    private fun forwardToDetailsView(packageName: String) {
        val intent = Intent(itemView.context, DetailsActivity::class.java).apply {
            putExtra(DetailsActivity.KEY_PACKAGE_NAME, packageName)
        }

        itemView.context.startActivity(intent)
    }

    private fun buildPropertiesString(appInfo: AppInfoEntity): String {
        val properties = mutableListOf<String>()

        if (appInfo.hasSystemAppFlag) {
            properties.add(itemView.context.getString(R.string.app_property_system_flag))
        }
        if (appInfo.targetSdk < 23) {
            properties.add(itemView.context.getString(R.string.app_property_old_target_sdk))
        }
        if (!appInfo.hasVisibilityToUser) {
            properties.add(itemView.context.getString(R.string.app_property_launcher_category))
        }
        if (appInfo.hasReadCallLog) {
            properties.add(itemView.context.getString(R.string.app_property_call_log))
        }
        if (appInfo.hasProcessOutgoingCalls) {
            properties.add(itemView.context.getString(R.string.app_property_outgoing_calls))
        }
        if (appInfo.hasReadContacts) {
            properties.add(itemView.context.getString(R.string.app_property_contacts))
        }
        if (appInfo.hasReadSms) {
            properties.add(itemView.context.getString(R.string.app_property_read_sms))
        }
        if (appInfo.hasReceiveSms) {
            properties.add(itemView.context.getString(R.string.app_property_receive_sms))
        }
        if (appInfo.hasLocationFinePermission || appInfo.hasLocationCoarsePermission) {
            properties.add(itemView.context.getString(R.string.app_property_location))
        }
        if (appInfo.hasNotificationListenerService) {
            properties.add(itemView.context.getString(R.string.app_property_notification_service))
        }
        if (appInfo.hasAccessibilityService) {
            properties.add(itemView.context.getString(R.string.app_property_accessibility_service))
        }
        if (appInfo.hasUnknownInstallerPackageName) {
            properties.add(itemView.context.getString(R.string.app_property_unknown_installer))
        }
        if (appInfo.hasRecordAudio) {
            properties.add(itemView.context.getString(R.string.app_property_record_audio))
        }
        if (appInfo.hasReadCalendar) {
            properties.add(itemView.context.getString(R.string.app_property_read_calendar))
        }
        if (appInfo.hasCamera) {
            properties.add(itemView.context.getString(R.string.app_property_camera))
        }
        if (appInfo.hasDeviceAdmin) {
            properties.add(itemView.context.getString(R.string.app_property_has_device_admin))
        }
        if (appInfo.hasInstallUnknownSource) {
            properties.add(itemView.context.getString(R.string.app_property_has_install_packages))
        }
        if (appInfo.hasModifySystemSettings) {
            properties.add(itemView.context.getString(R.string.app_property_has_modify_settings))
        }
        if (appInfo.hasSystemAlertWindow) {
            properties.add(itemView.context.getString(R.string.app_property_has_system_alert_window))
        }

        return if (properties.isNotEmpty()) {
            properties.joinToString(", ")
        } else {
            itemView.context.getString(R.string.app_property_empty)
        }
    }
}