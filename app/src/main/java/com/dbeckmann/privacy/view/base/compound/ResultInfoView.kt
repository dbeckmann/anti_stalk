package com.dbeckmann.privacy.view.base.compound

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.dbeckmann.privacy.R

class ResultInfoView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttr: Int = 0
) : ConstraintLayout(context, attributeSet, defStyleAttr) {

    private val title: TextView
    private val infoText: TextView
    private val icon: ImageView

    init {
        LayoutInflater.from(context).inflate(R.layout.layout_result_info, this, true)

        title = this.findViewById(R.id.tv_title)
        infoText = this.findViewById(R.id.tv_info_text)
        icon = this.findViewById(R.id.iv_result_icon)

        initView(attributeSet)
    }

    private fun initView(attributeSet: AttributeSet?) {
        attributeSet?.also {
            val attributes = context.obtainStyledAttributes(attributeSet, R.styleable.ResultInfoView)

            attributes.getString(R.styleable.ResultInfoView_titleText)?.also {
                setTitleText(it)
            }

            attributes.getString(R.styleable.ResultInfoView_infoText)?.also {
                setInfoText(it)
            }

            attributes.getDrawable(R.styleable.ResultInfoView_icon)?.also {
                setIcon(it)
            }

            attributes.recycle()
        }
    }

    fun setTitleText(@StringRes stringId: Int) {
        title.setText(stringId)
    }

    fun setTitleText(text: String) {
        title.setText(text)
    }

    fun setInfoText(@StringRes stringId: Int) {
        infoText.setText(stringId)
    }

    fun setInfoText(text: String) {
        infoText.setText(text)
    }

    fun setIcon(drawable: Drawable) {
        icon.setImageDrawable(drawable)
    }

    fun setIcon(drawableId: Int) {
        icon.setImageDrawable(context.getDrawable(drawableId))
    }
}