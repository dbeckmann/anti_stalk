package com.dbeckmann.privacy.view.detail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.di.Injector
import com.dbeckmann.privacy.di.activity.viewmodel.DaggerViewModelProviderFactory
import com.dbeckmann.privacy.view.base.TileDelegate
import com.dbeckmann.privacy.view.base.tile.BaseTileRecyclerViewAdapter
import javax.inject.Inject

class DetailsActivity : AppCompatActivity() {

    @Inject lateinit var viewModelProvider: DaggerViewModelProviderFactory
    private lateinit var detailsViewModel: DetailsViewModel

    private lateinit var toolbar: Toolbar
    private lateinit var rvAdapter: BaseTileRecyclerViewAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var recyclerView: RecyclerView
    private lateinit var tileDelegate: TileDelegate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        Injector.injectActivityComponent(this).inject(this)

        detailsViewModel = ViewModelProvider(this, viewModelProvider).get(DetailsViewModel::class.java)
        detailsViewModel.state.observe(this, Observer { state ->
            processState(state as DetailsState)
        })

        tileDelegate = TileDelegate(detailsViewModel)

        recyclerView = findViewById(R.id.rv_list)
        layoutManager = LinearLayoutManager(this)
        rvAdapter = BaseTileRecyclerViewAdapter(tileDelegate)

        recyclerView.adapter = rvAdapter
        recyclerView.layoutManager = layoutManager

        toolbar = findViewById(R.id.toolbar)

        intent.getStringExtra(KEY_PACKAGE_NAME)?.also {
            detailsViewModel.processApp(it)
        }

        toolbar.setTitle(R.string.details_name)
    }

    private fun processState(state: DetailsState) {
        when (state) {
            UpdatedTiles -> {
                rvAdapter.notifyDataSetChanged()
            }
        }
    }

    companion object {
        private const val TAG = "DetailsActivity"
        const val KEY_PACKAGE_NAME = "keyPackageName"
    }
}