package com.dbeckmann.privacy.view.result.tile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.view.base.tile.AbstractTileViewHolder
import com.dbeckmann.privacy.view.base.tile.Tile

class HeaderSubtextTile(private val data: HeaderSubtextData) : Tile<HeaderSubtextData> {
    override fun getItemViewType(): Int = R.id.headerSubtextTile

    override fun getData(callback: UseCaseCallback<HeaderSubtextData>) {
        callback.onSuccess(data)
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_header_subtext_tile, parent, false)
        return HeaderSubtextTileViewHolder(view)
    }
}

class HeaderSubtextTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private val title: TextView = itemView.findViewById(R.id.tv_title)
    private val subtext: TextView = itemView.findViewById(R.id.tv_subtext)

    override fun setTile(tile: Tile<Any>) {
        tile.getData(
            UseCaseCallback {
                val result = it as HeaderSubtextData
                render(result)
            }
        )
    }

    private fun render(data: HeaderSubtextData) {
        title.text = itemView.context.getString(data.titleId)
        subtext.text = itemView.context.getString(data.subtextId)
    }
}

data class HeaderSubtextData(
    val titleId: Int,
    val subtextId: Int
)