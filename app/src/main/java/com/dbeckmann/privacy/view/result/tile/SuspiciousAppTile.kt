package com.dbeckmann.privacy.view.result.tile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.view.apps.tile.AppTile
import com.dbeckmann.privacy.view.apps.tile.AppTileViewHolder
import com.dbeckmann.privacy.view.base.tile.AbstractTileViewHolder

class SuspiciousAppTile(
    appInfo: AppInfoEntity,
    private val buttonUninstallClick: (String) -> Unit
) : AppTile(appInfo) {
    override fun getItemViewType(): Int = R.id.suspiciousAppTile

    override fun getData(callback: UseCaseCallback<AppInfoEntity>) {
        callback.onSuccess(appInfo)
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_suspicious_tile, parent, false)
        return SuspiciousAppTileViewHolder(view, buttonUninstallClick)
    }
}

class SuspiciousAppTileViewHolder(itemView: View, val buttonInstallClick: (String) -> Unit) : AppTileViewHolder(itemView) {

    private var buttonUninstall: Button? = itemView.findViewById(R.id.b_uninstall)

    override fun render(data: AppInfoEntity) {
        super.render(data)

        buttonUninstall?.setOnClickListener {
            buttonInstallClick(data.packageName)
        }
    }
}