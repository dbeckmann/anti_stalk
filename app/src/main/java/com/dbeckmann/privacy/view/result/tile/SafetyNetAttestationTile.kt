package com.dbeckmann.privacy.view.result.tile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dbeckmann.domain.model.RootStateEntity
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.view.base.compound.ResultInfoView
import com.dbeckmann.privacy.view.base.tile.AbstractTileViewHolder
import com.dbeckmann.privacy.view.base.tile.Tile

class SafetyNetAttestationTile(private val rootState: RootStateEntity) : Tile<RootStateEntity> {
    override fun getItemViewType(): Int = R.id.rootStateTile

    override fun getData(callback: UseCaseCallback<RootStateEntity>) {
        callback.onSuccess(rootState)
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_safety_net_attestation_success, parent, false)
        return SafetyNetAttestationTileViewHolder(view)
    }
}

class SafetyNetAttestationTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private val resultView: ResultInfoView = itemView.findViewById(R.id.riv_safety_net_result)

    override fun setTile(tile: Tile<Any>) {
        tile.getData(UseCaseCallback
        {
            render(it as RootStateEntity)
        })
    }

    private fun render(rootState: RootStateEntity) {
        when (rootState.result) {
            RootStateEntity.Result.SUCCESS -> {
                resultView.setTitleText(R.string.main_safety_net_success_title)
                resultView.setInfoText(R.string.main_safety_net_success_text)
                resultView.setIcon(R.drawable.drawable_check)
            }
            RootStateEntity.Result.FAILED_ATTESTATION -> {
                resultView.setTitleText(R.string.main_safety_net_failed_title)
                resultView.setInfoText(R.string.main_safety_net_failed_text)
                resultView.setIcon(R.drawable.ic_error_outline_orange_24dp)
            }
            RootStateEntity.Result.FAILED_PLAY_SERVICES -> {
                resultView.setTitleText(R.string.main_safety_net_failed_play_services_title)
                resultView.setInfoText(R.string.main_safety_net_failed_play_services_text)
                resultView.setIcon(R.drawable.ic_error_outline_orange_24dp)
            }
            RootStateEntity.Result.FAILED_API -> {
                resultView.setTitleText(R.string.main_safety_net_failed_api_title)
                resultView.setInfoText(R.string.main_safety_net_failed_api_text)
                resultView.setIcon(R.drawable.ic_error_outline_orange_24dp)
            }
        }
    }
}