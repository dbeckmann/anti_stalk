package com.dbeckmann.privacy.view.detail

import com.dbeckmann.privacy.R

object PropertyMap {
    const val PROPERTY_OLD_TARGET_SDK = "property.OLD_TARGET_SDK"
    const val PROPERTY_ACCESSIBILITY_SERVICE = "property.ACCESSIBILITY_SERVICE"
    const val PROPERTY_NOTIFICATION_LISTENER_SERIVCE = "property.NOTIFICATION_LISTENER_SERVICE"
    const val PROPERTY_UNKNOWN_INSTALLER = "property.UNKNOWN_INSTALLER"
    const val PROPERTY_NO_CATEGORY_LAUNCHER = "property.NO_CATEGORY_LAUNCHER"
    const val PROPERTY_ACTIVE_DEVICE_ADMIN = "property.ACTIVE_DEVICE_ADMIN"
    const val PROPERTY_HAS_DEVICE_ADMIN = "property.HAS_DEVICE_ADMIN"
    const val PROPERTY_HAS_INSTALL_PACKAGES = "property.HAS_INSTALL_PACKAGES"
    const val PROPERTY_HAS_MODIFY_SETTINGS = "property.HAS_MODIFY_SETTINGS"
    const val PROPERTY_HAS_SYSTEM_ALERT_WINDOW = "property.HAS_SYSTEM_ALERT_WINDOW"
    const val PROPERTY_CALL_NETWORK_ACCESS = "property.CALL_NETWORK_ACCESS"
    const val PROPERTY_ADVANCED_SMS_ACCESS = "property.ADVANCED_SMS_ACCESS"

    val map = hashMapOf(
        Pair(
            PROPERTY_OLD_TARGET_SDK,
            PropertyDetail(
                R.drawable.ic_perm_device_information_black_24dp,
                R.string.property_name_old_target_sdk,
                R.string.property_description_old_target_sdk
            )
        ),
        Pair(
            PROPERTY_ACCESSIBILITY_SERVICE,
            PropertyDetail(
                R.drawable.ic_accessibility_black_24dp,
                R.string.property_name_accessibility,
                R.string.property_description_accessibility
            )
        ),
        Pair(
            PROPERTY_NOTIFICATION_LISTENER_SERIVCE,
            PropertyDetail(
                R.drawable.ic_notifications_black_24dp,
                R.string.property_name_notification_listener,
                R.string.property_description_notification_listener
            )
        ),
        Pair(
            PROPERTY_UNKNOWN_INSTALLER,
            PropertyDetail(
                R.drawable.ic_adb_black_24dp,
                R.string.property_name_unknown_installer,
                R.string.property_description_unknown_installer
            )
        ),
        Pair(
            PROPERTY_NO_CATEGORY_LAUNCHER,
            PropertyDetail(
                R.drawable.ic_visibility_off_black_24dp,
                R.string.property_name_no_category_launcher,
                R.string.property_description_no_category_launcher
            )
        ),
        Pair(
            PROPERTY_ACTIVE_DEVICE_ADMIN,
            PropertyDetail(
                R.drawable.ic_phonelink_setup_black_24dp,
                R.string.property_name_active_device_admin,
                R.string.property_description_active_device_admin
            )
        ),
        Pair(
            PROPERTY_HAS_DEVICE_ADMIN,
            PropertyDetail(
                R.drawable.ic_phonelink_setup_black_24dp,
                R.string.property_name_device_admin,
                R.string.property_description_device_admin
            )
        ),
        Pair(
            PROPERTY_HAS_INSTALL_PACKAGES,
            PropertyDetail(
                R.drawable.ic_get_app_black_24dp,
                R.string.property_name_install_packages,
                R.string.property_description_install_packages
            )
        ),
        Pair(
            PROPERTY_HAS_MODIFY_SETTINGS,
            PropertyDetail(
                R.drawable.ic_settings_black_24dp,
                R.string.property_name_modify_settings,
                R.string.property_description_modify_settings
            )
        ),
        Pair(
            PROPERTY_HAS_SYSTEM_ALERT_WINDOW,
            PropertyDetail(
                R.drawable.ic_aspect_ratio_black_24dp,
                R.string.property_name_system_alert_window,
                R.string.property_description_system_alert_window
            )
        ),
        Pair(
            PROPERTY_CALL_NETWORK_ACCESS,
            PropertyDetail(
                R.drawable.ic_phone_black_24dp,
                R.string.property_name_call_network_access,
                R.string.property_description_call_network_access
            )
        ),
        Pair(
            PROPERTY_ADVANCED_SMS_ACCESS,
            PropertyDetail(
                R.drawable.ic_sms_black_24dp,
                R.string.property_name_sms_access,
                R.string.property_description_sms_access
            )
        )
    )
}

data class PropertyDetail(
    val drawableId: Int,
    val nameId: Int,
    val descriptionId: Int
)