package com.dbeckmann.privacy.view.base

import androidx.recyclerview.widget.DiffUtil
import com.dbeckmann.privacy.view.base.tile.*

class TileDelegate(
    private val tileDataHolder: TileDataHolder
) : BaseTilePresenter {

    private val oldList: MutableList<Tile<Any>> = mutableListOf()

    override fun getItemCount(): Int = tileDataHolder.tiles.size
    override fun onBindViewHolder(tileItem: TileItem, pos: Int) = tileItem.setTile(tileDataHolder.tiles[pos])
    override fun getItemViewType(pos: Int): Int = tileDataHolder.tiles[pos].getItemViewType()
    override fun getTileByItemViewType(itemViewType: Int): Tile<Any> = tileDataHolder.tiles.first { it.getItemViewType() == itemViewType }

    fun updateList(adapter: BaseTileRecyclerViewAdapter) {
        val rs = DiffUtil.calculateDiff(TileDiffUtil(oldList, tileDataHolder.tiles))
        rs.dispatchUpdatesTo(adapter)

        oldList.apply {
            clear()
            addAll(tileDataHolder.tiles)
        }
    }

    class TileDiffUtil(
        private val oldList: List<Tile<Any>>,
        private val newList: List<Tile<Any>>
    ) : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition].tilesAreSame(newList[newItemPosition])

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldList[oldItemPosition] == newList[newItemPosition]

        override fun getOldListSize(): Int = oldList.size
        override fun getNewListSize(): Int = newList.size
    }
}