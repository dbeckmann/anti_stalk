package com.dbeckmann.privacy.view.result.tile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.view.base.tile.AbstractTileViewHolder
import com.dbeckmann.privacy.view.base.tile.Tile
import com.dbeckmann.domain.model.RootStateEntity
import com.dbeckmann.interactor.usecase.UseCaseCallback

class PhotoAppLocationEnabledTile(private val appInfoEntities: List<AppInfoEntity>) : Tile<List<AppInfoEntity>> {
    override fun getItemViewType(): Int = R.id.photoAppLocationEnabledTile

    override fun getData(callback: UseCaseCallback<List<AppInfoEntity>>) {
        callback.onSuccess(appInfoEntities)
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_photo_apps_location, parent, false)
        return PhotoAppLocationEnabledTileViewHolder(view)
    }
}

class PhotoAppLocationEnabledTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {
    override fun setTile(tile: Tile<Any>) {
        // do nothing
    }
}