package com.dbeckmann.privacy.view.detail

import com.dbeckmann.privacy.view.base.viewmodel.BaseState

sealed class DetailsState : BaseState
object UpdatedTiles : DetailsState()