package com.dbeckmann.privacy.view.apps

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.di.Injector
import com.dbeckmann.privacy.di.activity.viewmodel.DaggerViewModelProviderFactory
import com.dbeckmann.privacy.view.base.TileDelegate
import com.dbeckmann.privacy.view.base.tile.BaseTileRecyclerViewAdapter
import com.dbeckmann.privacy.view.result.tile.MainViewModel
import javax.inject.Inject

class AllAppsFragment : Fragment() {

    @Inject lateinit var viewModelProvider: DaggerViewModelProviderFactory

    private lateinit var allAppsViewModel: AllAppsViewModel
    private lateinit var mainViewModel: MainViewModel

    private lateinit var rvAdapter: BaseTileRecyclerViewAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var recyclerView: RecyclerView
    private lateinit var tileDelegate: TileDelegate

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.also {
            Injector.injectActivityComponent(it).inject(this)

            allAppsViewModel = ViewModelProvider(this, viewModelProvider).get(AllAppsViewModel::class.java)
            mainViewModel = ViewModelProvider(it, viewModelProvider).get(MainViewModel::class.java)

            tileDelegate = TileDelegate(allAppsViewModel)

            layoutManager = LinearLayoutManager(context)
            rvAdapter = BaseTileRecyclerViewAdapter(tileDelegate)
            recyclerView.adapter = rvAdapter
            recyclerView.layoutManager = layoutManager

            // observe state for this fragments viewmodel
            allAppsViewModel.state.observe(viewLifecycleOwner, Observer { state -> processState(state as AllAppsState) })

            // observe new results in mainViewModel
            mainViewModel.result.observe(viewLifecycleOwner, Observer { result -> allAppsViewModel.updateResults(result) })
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_all_apps, container, false)

        recyclerView = v.findViewById(R.id.rv_list)

        return v
    }

    private fun processState(state: AllAppsState) {
        when (state) {
            is UpdatedTiles -> tileDelegate.updateList(rvAdapter)
        }
    }

    companion object {
        const val KEY_PACKAGE_NAMES = "keyPackageNames"

        fun newInstance(packageNames: ArrayList<String>): AllAppsFragment = AllAppsFragment().apply {
            arguments = Bundle().apply {
                putStringArrayList(KEY_PACKAGE_NAMES, packageNames)
            }
        }
    }
}