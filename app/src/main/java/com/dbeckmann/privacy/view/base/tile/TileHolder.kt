package com.dbeckmann.privacy.view.base.tile

interface TileDataHolder {
    val tiles: List<Tile<Any>>
}