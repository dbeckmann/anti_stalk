package com.dbeckmann.privacy.view.detail

import android.Manifest
import com.dbeckmann.privacy.R

object PermissionMap {
    val CUSTOM_PLATFORM_PERMISSION_DETAILS = mapOf(
        Pair(Manifest.permission_group.CALENDAR, PermissionDetail(R.drawable.ic_event_black_24dp)),
        Pair(Manifest.permission_group.CALL_LOG, PermissionDetail(R.drawable.ic_call_black_24dp)),
        Pair(Manifest.permission_group.CAMERA, PermissionDetail(R.drawable.ic_photo_camera_black_24dp)),
        Pair(Manifest.permission_group.CONTACTS, PermissionDetail(R.drawable.ic_contacts_black_24dp)),
        Pair(Manifest.permission_group.LOCATION, PermissionDetail(R.drawable.ic_location_on_black_24dp)),
        Pair(Manifest.permission_group.MICROPHONE, PermissionDetail(R.drawable.ic_mic_none_black_24dp)),
        Pair(Manifest.permission_group.PHONE, PermissionDetail(R.drawable.ic_call_black_24dp)),
        Pair(Manifest.permission_group.SENSORS, PermissionDetail(R.drawable.ic_visibility_black_24dp)),
        Pair(Manifest.permission_group.SMS, PermissionDetail(R.drawable.ic_sms_black_24dp)),
        Pair(Manifest.permission_group.STORAGE, PermissionDetail(R.drawable.ic_folder_open_black_24dp))
    )

    // this must be mapped because Android 10 will not give out group name anymore, instead just android.permission-group.UNDEFINED
    // this list is received from
    // AOSP -> https://android.googlesource.com/platform/packages/apps/PackageInstaller/+/refs/heads/master/src/com/android/packageinstaller/permission/utils/Utils.java
    val PLATFORM_PERMISSIONS = mapOf(
        Pair(Manifest.permission.READ_CONTACTS, Manifest.permission_group.CONTACTS),
        Pair(Manifest.permission.WRITE_CONTACTS, Manifest.permission_group.CONTACTS),
        Pair(Manifest.permission.GET_ACCOUNTS, Manifest.permission_group.CONTACTS),
        Pair(Manifest.permission.READ_CALENDAR, Manifest.permission_group.CALENDAR),
        Pair(Manifest.permission.WRITE_CALENDAR, Manifest.permission_group.CALENDAR),
        Pair(Manifest.permission.SEND_SMS, Manifest.permission_group.SMS),
        Pair(Manifest.permission.RECEIVE_SMS, Manifest.permission_group.SMS),
        Pair(Manifest.permission.READ_SMS, Manifest.permission_group.SMS),
        Pair(Manifest.permission.RECEIVE_MMS, Manifest.permission_group.SMS),
        Pair(Manifest.permission.RECEIVE_WAP_PUSH, Manifest.permission_group.SMS),
        //Pair(Manifest.permission.READ_CELL_BROADCASTS, Manifest.permission_group.SMS),
        Pair(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission_group.STORAGE),
        Pair(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission_group.STORAGE),
        Pair(Manifest.permission.ACCESS_MEDIA_LOCATION, Manifest.permission_group.STORAGE),
        Pair(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission_group.LOCATION),
        Pair(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission_group.LOCATION),
        Pair(Manifest.permission.ACCESS_BACKGROUND_LOCATION, Manifest.permission_group.LOCATION),
        Pair(Manifest.permission.READ_CALL_LOG, Manifest.permission_group.CALL_LOG),
        Pair(Manifest.permission.WRITE_CALL_LOG, Manifest.permission_group.CALL_LOG),
        Pair(Manifest.permission.PROCESS_OUTGOING_CALLS, Manifest.permission_group.CALL_LOG),
        Pair(Manifest.permission.READ_PHONE_STATE, Manifest.permission_group.PHONE),
        Pair(Manifest.permission.READ_PHONE_NUMBERS, Manifest.permission_group.PHONE),
        Pair(Manifest.permission.CALL_PHONE, Manifest.permission_group.PHONE),
        Pair(Manifest.permission.ADD_VOICEMAIL, Manifest.permission_group.PHONE),
        Pair(Manifest.permission.USE_SIP, Manifest.permission_group.PHONE),
        Pair(Manifest.permission.ANSWER_PHONE_CALLS, Manifest.permission_group.PHONE),
        Pair(Manifest.permission.ACCEPT_HANDOVER, Manifest.permission_group.PHONE),
        Pair(Manifest.permission.RECORD_AUDIO, Manifest.permission_group.MICROPHONE),
        Pair(Manifest.permission.ACTIVITY_RECOGNITION, Manifest.permission_group.ACTIVITY_RECOGNITION),
        Pair(Manifest.permission.CAMERA, Manifest.permission_group.CAMERA),
        Pair(Manifest.permission.BODY_SENSORS, Manifest.permission_group.SENSORS)
    )
}

data class PermissionDetail(
    val drawableId: Int
)