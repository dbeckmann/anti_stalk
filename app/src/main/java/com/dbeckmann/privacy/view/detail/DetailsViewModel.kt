package com.dbeckmann.privacy.view.detail

import android.Manifest
import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.usecase.GetAppInfoEntityByPackageName
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.util.Logger
import com.dbeckmann.privacy.view.base.tile.Tile
import com.dbeckmann.privacy.view.base.tile.TileDataHolder
import com.dbeckmann.privacy.view.base.viewmodel.BaseViewModel
import com.dbeckmann.privacy.view.detail.tile.AppInfoTile
import com.dbeckmann.privacy.view.detail.tile.PermissionTile
import com.dbeckmann.privacy.view.detail.tile.PropertyTile
import com.dbeckmann.privacy.view.result.tile.CenteredTextTile
import com.dbeckmann.privacy.view.result.tile.HeaderSubtextData
import com.dbeckmann.privacy.view.result.tile.HeaderSubtextTile
import com.dbeckmann.privacy.view.result.tile.HeaderTile
import javax.inject.Inject

class DetailsViewModel @Inject constructor(
    private val logger: Logger,
    private val getAppInfoEntity: GetAppInfoEntityByPackageName
) : BaseViewModel(), TileDataHolder {

    override val tiles: MutableList<Tile<Any>> = mutableListOf()

    fun processApp(packageName: String) {
        logger.d(TAG, "processApp")

        getAppInfoEntity.execute(packageName) {
            buildTiles(it)
        }
    }

    private fun buildTiles(appInfoEntity: AppInfoEntity) {
        logger.d(TAG, "buildTiles")

        tiles.add(AppInfoTile(appInfoEntity))

        tiles.add(HeaderTile(R.string.details_suspicious_properties_header))
        addSuspiciousPropertyTiles(appInfoEntity)

        tiles.add(HeaderSubtextTile(HeaderSubtextData(R.string.details_permission_dangerous_header, R.string.details_permission_dangerous_subtext)))
        addDangerousPermissionTiles(appInfoEntity)

        tiles.add(HeaderSubtextTile(HeaderSubtextData(R.string.details_permission_normal_header, R.string.details_permission_normal_subtext)))
        addNormalPermissionTiles(appInfoEntity)

        setState(UpdatedTiles)
    }

    private fun addDangerousPermissionTiles(appInfoEntity: AppInfoEntity) {
        val dangerousPermissionTiles = mutableListOf<PermissionTile>()
        appInfoEntity.permissions
            .filter { it.isAndroid && it.isDangerous }
            .groupBy { it.group }
            .toSortedMap(compareBy { PERMISSION_GROUP_DISPLAY_ORDER.indexOf(it) })
            .forEach { map ->
                map.value
                    .sortedByDescending { it.isGranted }
                    .forEach {
                        dangerousPermissionTiles.add(PermissionTile(it))
                    }
            }

        if (dangerousPermissionTiles.isNotEmpty()) {
            tiles.addAll(dangerousPermissionTiles)
        } else {
            tiles.add(CenteredTextTile(R.string.details_no_dangerous_permissions))
        }
    }

    private fun addNormalPermissionTiles(appInfoEntity: AppInfoEntity) {
        val dangerousPermissionTiles = mutableListOf<PermissionTile>()
        appInfoEntity.permissions
            .filter { it.isAndroid && !it.isDangerous }
            .sortedByDescending { it.isGranted }
            .forEach {
                dangerousPermissionTiles.add(PermissionTile(it))
            }

        if (dangerousPermissionTiles.isNotEmpty()) {
            tiles.addAll(dangerousPermissionTiles)
        } else {
            tiles.add(CenteredTextTile(R.string.details_no_permissions))
        }
    }

    private fun addSuspiciousPropertyTiles(appInfoEntity: AppInfoEntity) {
        val propertyTiles = mutableListOf<PropertyTile>()

        if (appInfoEntity.hasUnknownInstallerPackageName) {
            propertyTiles.add(PropertyTile(PropertyMap.PROPERTY_UNKNOWN_INSTALLER))
        }
        if (!appInfoEntity.hasVisibilityToUser) {
            propertyTiles.add(PropertyTile(PropertyMap.PROPERTY_NO_CATEGORY_LAUNCHER))
        }
        if (appInfoEntity.targetSdk < 23) {
            propertyTiles.add(PropertyTile(PropertyMap.PROPERTY_OLD_TARGET_SDK))
        }
        if (appInfoEntity.hasProcessOutgoingCalls || appInfoEntity.hasReadCallLog || appInfoEntity.hasReadPhoneState) {
            propertyTiles.add(PropertyTile(PropertyMap.PROPERTY_CALL_NETWORK_ACCESS))
        }
        if (appInfoEntity.hasReadSms || appInfoEntity.hasReceiveSms) {
            propertyTiles.add(PropertyTile(PropertyMap.PROPERTY_ADVANCED_SMS_ACCESS))
        }
        if (appInfoEntity.hasNotificationListenerService) {
            propertyTiles.add(PropertyTile(PropertyMap.PROPERTY_NOTIFICATION_LISTENER_SERIVCE))
        }
        if (appInfoEntity.hasAccessibilityService) {
            propertyTiles.add(PropertyTile(PropertyMap.PROPERTY_ACCESSIBILITY_SERVICE))
        }
        if (appInfoEntity.hasActiveDeviceAdmin) {
            propertyTiles.add(PropertyTile(PropertyMap.PROPERTY_ACTIVE_DEVICE_ADMIN))
        }
        if (appInfoEntity.hasDeviceAdmin && !appInfoEntity.hasActiveDeviceAdmin) {
            propertyTiles.add(PropertyTile(PropertyMap.PROPERTY_HAS_DEVICE_ADMIN))
        }
        if (appInfoEntity.hasInstallUnknownSource) {
            propertyTiles.add(PropertyTile(PropertyMap.PROPERTY_HAS_INSTALL_PACKAGES))
        }
        if (appInfoEntity.hasModifySystemSettings) {
            propertyTiles.add(PropertyTile(PropertyMap.PROPERTY_HAS_MODIFY_SETTINGS))
        }
        if (appInfoEntity.hasSystemAlertWindow) {
            propertyTiles.add(PropertyTile(PropertyMap.PROPERTY_HAS_SYSTEM_ALERT_WINDOW))
        }

        if (propertyTiles.isNotEmpty()) {
            tiles.addAll(propertyTiles)
        } else {
            tiles.add(CenteredTextTile(R.string.details_no_suspicious_properties))
        }
    }

    companion object {
        const val TAG = "DetailsViewModel"

        val PERMISSION_GROUP_DISPLAY_ORDER = listOf(
            Manifest.permission_group.CALL_LOG,
            Manifest.permission_group.PHONE,
            Manifest.permission_group.SMS,
            Manifest.permission_group.CONTACTS,
            Manifest.permission_group.CALENDAR,
            Manifest.permission_group.LOCATION,
            Manifest.permission_group.MICROPHONE,
            Manifest.permission_group.CAMERA,
            Manifest.permission_group.SENSORS,
            Manifest.permission_group.ACTIVITY_RECOGNITION,
            Manifest.permission_group.STORAGE
        )
    }
}