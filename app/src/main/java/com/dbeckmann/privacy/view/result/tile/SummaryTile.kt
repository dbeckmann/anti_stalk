package com.dbeckmann.privacy.view.result.tile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dbeckmann.domain.model.RootStateEntity
import com.dbeckmann.interactor.usecase.UseCaseCallback
import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.util.setVisible
import com.dbeckmann.privacy.view.base.tile.AbstractTileViewHolder
import com.dbeckmann.privacy.view.base.tile.Tile

class SummaryTile(val data: SummaryTileData) : Tile<SummaryTileData> {
    override fun getItemViewType(): Int = R.id.summaryTile

    override fun getData(callback: UseCaseCallback<SummaryTileData>) {
        callback.onSuccess(data)
    }

    override fun createViewHolder(parent: ViewGroup): AbstractTileViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_summary, parent, false)
        return SummaryTileViewHolder(view)
    }
}

class SummaryTileViewHolder(itemView: View) : AbstractTileViewHolder(itemView) {

    private val container = itemView.findViewById<View>(R.id.cl_container)
    private val icon = itemView.findViewById<ImageView>(R.id.iv_result_icon)
    private val header = itemView.findViewById<TextView>(R.id.tv_header)
    private val infoText = itemView.findViewById<TextView>(R.id.tv_info_text)

    override fun setTile(tile: Tile<Any>) {
        tile.getData(
            UseCaseCallback {
                val result = it as SummaryTileData
                render(result)
            }
        )
    }

    private fun render(data: SummaryTileData) {
        when {
            data.numSuspiciousApps > 0 && data.getNumOtherIssues() > 0 -> {
                container.setBackgroundColor(itemView.context.resources.getColor(R.color.warningBackground))
                icon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_security_black_24dp))
                header.text = itemView.context.getString(R.string.summary_header_issues_text)
                infoText.text = itemView.context.getString(R.string.summary_info_issues_text_1, data.numSuspiciousApps, data.getNumOtherIssues())
                infoText.setVisible()
            }

            data.numSuspiciousApps > 0 && data.getNumOtherIssues() == 0 -> {
                container.setBackgroundColor(itemView.context.resources.getColor(R.color.warningBackground))
                icon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_security_black_24dp))
                header.text = itemView.context.getString(R.string.summary_header_issues_text)
                infoText.text = itemView.context.getString(R.string.summary_info_issues_text_2, data.numSuspiciousApps)
                infoText.setVisible()
            }

            data.numSuspiciousApps == 0 && data.getNumOtherIssues() > 0 -> {
                container.setBackgroundColor(itemView.context.resources.getColor(R.color.warningBackground))
                icon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_security_black_24dp))
                header.text = itemView.context.getString(R.string.summary_header_issues_text)
                infoText.text = itemView.context.getString(R.string.summary_info_issues_text_3, data.getNumOtherIssues())
                infoText.setVisible()
            }

            // no problems found
            data.numSuspiciousApps == 0 && data.getNumOtherIssues() == 0 -> {
                container.setBackgroundColor(itemView.context.resources.getColor(R.color.sane))
                icon.setImageDrawable(itemView.context.getDrawable(R.drawable.ic_verified_black_24dp))
                header.text = itemView.context.getString(R.string.summary_header_no_issues_text)
                infoText.text = itemView.context.getString(R.string.summary_info_no_issues_text)
            }
        }
    }
}

data class SummaryTileData(
    val numSuspiciousApps: Int,
    val rootState: RootStateEntity,
    val safetyNetEnabled: Boolean,
    val keyguardSecured: Boolean
) {
    fun getNumOtherIssues(): Int {
        var numIssues = 0
        if (!keyguardSecured) {
            numIssues++
        }
        if (!safetyNetEnabled) {
            numIssues++
        }
        if (rootState.result != RootStateEntity.Result.SUCCESS) {
            numIssues++
        }

        return numIssues
    }
}