package com.dbeckmann.privacy.view.result

import com.dbeckmann.privacy.R
import com.dbeckmann.privacy.data.DeviceScanResult
import com.dbeckmann.privacy.util.Logger
import com.dbeckmann.privacy.view.base.tile.Tile
import com.dbeckmann.privacy.view.base.tile.TileDataHolder
import com.dbeckmann.privacy.view.base.viewmodel.BaseViewModel
import com.dbeckmann.privacy.view.result.tile.*
import javax.inject.Inject

class ResultViewModel @Inject constructor(
    private val logger: Logger
) : BaseViewModel(), TileDataHolder {

    override val tiles: MutableList<Tile<Any>> = mutableListOf()
    private var deviceScanResult: DeviceScanResult? = null

    fun updateResults(result: DeviceScanResult) {
        deviceScanResult = result
        buildTiles()
    }

    private fun buildTiles() {
        tiles.clear()

        deviceScanResult?.also { result ->
            // add summary
            tiles.add(
                SummaryTile(
                    SummaryTileData(
                        numSuspiciousApps = result.suspiciousApps.size,
                        rootState = result.attestationResult,
                        safetyNetEnabled = result.safetyNetEnabled,
                        keyguardSecured = result.hasKeyguardSecured
                    )
                )
            )

            // create suspicious apps
            tiles.add(HeaderTile(R.string.header_suspicious_apps))

            if (result.suspiciousApps.isNotEmpty()) {
                result.suspiciousApps
                    .distinct()
                    .sortedByDescending { it.score }
                    .forEach { tiles.add(SuspiciousAppTile(it, this::buttonInstallClick)) }
            } else {
                tiles.add(NoSuspiciousAppTile(result.installedApps.size, this::buttonViewResults))
            }

            tiles.add(HeaderTile(R.string.header_device_state))

            tiles.add(SafetyNetAttestationTile(result.attestationResult))
            tiles.add(SafetyNetEnabledTile(result.safetyNetEnabled))
            tiles.add(KeyguardSecuredTile(result.hasKeyguardSecured))

            if (result.locationEnabledPhotoApps.isNotEmpty()) {
                tiles.add(PhotoAppLocationEnabledTile(result.locationEnabledPhotoApps))
            }

            setState(UpdatedTiles)
        }
    }

    private fun buttonInstallClick(packageName: String) = setState(ButtonUninstallClicked(packageName))
    private fun buttonViewResults() = setState(ButtonViewResults)

    companion object {
        const val TAG = "ResultViewModel"
    }
}