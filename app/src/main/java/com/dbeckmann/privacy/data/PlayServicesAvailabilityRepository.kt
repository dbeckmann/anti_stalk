package com.dbeckmann.privacy.data

interface PlayServicesAvailabilityRepository {
    fun isAvailable(): Boolean
}