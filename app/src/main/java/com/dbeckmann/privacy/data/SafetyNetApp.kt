package com.dbeckmann.privacy.data

import android.content.Context
import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.repository.AppScanRepository
import com.dbeckmann.domain.usecase.GetInstalledApps
import com.dbeckmann.interactor.usecase.Executor
import com.google.android.gms.safetynet.HarmfulAppsData
import com.google.android.gms.safetynet.SafetyNet
import javax.inject.Inject

class SafetyNetApp @Inject constructor(
    private val context: Context,
    private val getInstalledApps: GetInstalledApps
) : AppScanRepository {

    override fun getInstalledApps(): Executor<List<AppInfoEntity>> = Executor {
        // not implemented here
    }

    override fun getInstalledAppsWithScores(): Executor<List<AppInfoEntity>> = Executor {
        // not implemented here
    }

    override fun getSuspiciousApps(): Executor<List<AppInfoEntity>> = Executor { executor ->
        try {
            val installedApps = mutableListOf<AppInfoEntity>()
            val harmfulApps = mutableListOf<AppInfoEntity>()

            getInstalledApps.execute {
                installedApps.addAll(it)
            }

            SafetyNet.getClient(context)
                .listHarmfulApps()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        task.result?.harmfulAppsList?.also {
                            harmfulApps.addAll(mapToAppInfoEntity(it, installedApps))
                        }
                    }
                }
            executor.onSuccess(harmfulApps)
        } catch (e: Exception) {
            executor.onError(e)
        }
    }

    override fun getAppInfoEntityByPackageName(packageName: String): Executor<AppInfoEntity> = Executor {
        // not implemented
    }

    override fun getAppInfoEntityByListPackageName(packageName: List<String>): Executor<List<AppInfoEntity>> = Executor {
        // not implemented
    }

    /**
     * @TODO this currently just removes not found packageNames from HarmfulAppsData -> should be reconsidered
     */
    private fun mapToAppInfoEntity(harmfulAppsData: List<HarmfulAppsData>, installedApps: List<AppInfoEntity>): List<AppInfoEntity> =
        harmfulAppsData.map { hApps ->
            installedApps.find { it.packageName == hApps.apkPackageName }
        }.filterNotNull()
}