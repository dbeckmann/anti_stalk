package com.dbeckmann.privacy.data.properties

import com.dbeckmann.domain.model.AppInfoEntity

class PropertyProcessor<T : SuspiciousProperty>(
    private vararg val propertyProcessors: T
) {
    fun calcScore(appInfo: AppInfoEntity): Score {
        var score = 0F

        propertyProcessors.forEach {
            score += it.calcScore(appInfo)
        }

        val sampleScore = score
        val confidenceScore = calcConfidenceScore(sampleScore)

        return Score(
            score = sampleScore,
            confidenceScore = confidenceScore
        )
    }

    private fun getMaxScore(): Float {
        var maxScore = 0F

        propertyProcessors.forEach {
            if (!it.isDownRating) {
                maxScore += it.score
            }
        }

        return maxScore
    }

    private fun calcConfidenceScore(score: Float): Float {
        val referenceScores = listOf(20F, 12F)

        var sum = 0F
        referenceScores.forEach {
            sum += score / it
        }

        return sum / referenceScores.size
    }

    data class Score(
        val score: Float,
        val confidenceScore: Float
    )
}