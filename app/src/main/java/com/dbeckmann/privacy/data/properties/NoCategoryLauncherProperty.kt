package com.dbeckmann.privacy.data.properties

import com.dbeckmann.domain.model.AppInfoEntity

class NoCategoryLauncherProperty : SuspiciousProperty {

    // a non system app without launcher category has a high chance of deceptive behaviour
    // therefore apps doing this will be scored higher
    override val score: Float = 2F
    override val isDownRating: Boolean = false

    override fun calcScore(appInfo: AppInfoEntity): Float =
        if (!appInfo.hasSystemAppFlag && !appInfo.hasVisibilityToUser) {
            score
        } else {
            0F
        }
}