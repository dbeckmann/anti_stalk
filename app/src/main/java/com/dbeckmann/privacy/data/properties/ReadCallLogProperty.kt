package com.dbeckmann.privacy.data.properties

import com.dbeckmann.domain.model.AppInfoEntity

class ReadCallLogProperty : SuspiciousProperty {
    override val score: Float = 2F
    override val isDownRating: Boolean = false

    override fun calcScore(appInfo: AppInfoEntity): Float =
        if (appInfo.hasReadCallLog && !appInfo.hasDialerAction) {
            score
        } else {
            0F
        }
}