package com.dbeckmann.privacy.data.properties

import com.dbeckmann.domain.model.AppInfoEntity

class ReadContactProperty : SuspiciousProperty {
    override val score: Float = 1F
    override val isDownRating: Boolean = false

    override fun calcScore(appInfo: AppInfoEntity): Float =
        if (appInfo.hasReadContacts) {
            score
        } else {
            0F
        }
}