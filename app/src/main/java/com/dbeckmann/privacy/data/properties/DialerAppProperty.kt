package com.dbeckmann.privacy.data.properties

import com.dbeckmann.domain.model.AppInfoEntity

class DialerAppProperty : SuspiciousProperty {

    // dialer apps need to access a lot of dangerous permissions
    // therefore all known dialer apps on a device will be rated down
    override val score: Float = -2F
    override val isDownRating: Boolean = true

    override fun calcScore(appInfo: AppInfoEntity): Float =
        if (appInfo.hasDialerAction) {
            score
        } else {
            0F
        }
}