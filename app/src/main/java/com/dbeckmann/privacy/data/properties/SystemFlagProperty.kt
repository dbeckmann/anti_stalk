package com.dbeckmann.privacy.data.properties

import com.dbeckmann.domain.model.AppInfoEntity

class SystemFlagProperty : SuspiciousProperty {

    // system apps can have, for good reasons, many suspicious app properties
    // therefore the score for system apps will be decreased a little bit
    override val score: Float = -2F
    override val isDownRating: Boolean = true

    override fun calcScore(appInfo: AppInfoEntity): Float =
        if (appInfo.hasSystemAppFlag) {
            score
        } else {
            0F
        }
}