package com.dbeckmann.privacy.data

import android.content.Context
import android.content.pm.PackageManager
import android.util.Base64
import com.dbeckmann.privacy.BuildConfig
import com.dbeckmann.privacy.util.Logger
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.safetynet.SafetyNet
import org.json.JSONArray
import org.json.JSONObject
import org.spongycastle.asn1.x500.X500Name
import org.spongycastle.asn1.x500.style.BCStyle
import java.io.FileInputStream
import java.security.DigestInputStream
import java.security.KeyStore
import java.security.MessageDigest
import java.security.Signature
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.util.*
import javax.inject.Inject
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

class SafetyNetAttestation @Inject constructor(
    val context: Context,
    val logger: Logger
) {
    private var result = Result.INITIAL

    private var nonce: String? = null
    private var jwsParts: List<String>? = null
    private var certificates = mutableListOf<X509Certificate>()
    private var signature: ByteArray? = null
    private var payload: Payload? = null

    fun attest(callback: (AttestationResult) -> Unit) {
        logger.d(TAG, "attest device")

        val tmpNonce = createNonce()
        nonce = tmpNonce

        SafetyNet.getClient(context).attest(tmpNonce.toByteArray(), BuildConfig.SAFETY_NET_API_KEY)
            .addOnSuccessListener { response ->
                logger.d(TAG, "attestation success")
                validateResponse(response.jwsResult)

                callback(AttestationResult(payload, result))
            }
            .addOnFailureListener {
                logger.e(TAG, "attestation failed")
                it.printStackTrace()

                result = if (it is ApiException) {
                    when (it.statusCode) {
                        7 -> Result.FAILED_NETWORK
                        17 -> Result.FAILED_PLAY_SERVICES
                        else -> Result.FAILED_API
                    }
                } else {
                    Result.FAILED_API
                }

                callback(AttestationResult(payload, result))
            }
    }

    private fun createNonce(): String = UUID.randomUUID().toString()

    private fun validateResponse(raw: String) {
        jwsParts = raw.split(".")

        jwsParts?.also { jws ->
            if (!isValidJws(jws)) {
                result = Result.FAILED_NO_VALID_JWS_RESPONSE
                return
            }

            if (!parseJwsHeader(jws[0])) {
                result = Result.FAILED_NO_VALID_JWS_HEADER
                return
            }

            if (!parseJwsPayload(jws[1])) {
                result = Result.FAILED_NO_VALID_PAYLOAD_NOT_READ
                return
            }

            parseJwsSignature(jws[2])

            if (!validatePayload()) {
                return
            }

            result = Result.SUCCESS
        } //@TODO we need a default error result
    }

    private fun validatePayload(): Boolean {
        if (!verifyCertificate()) {
            result = Result.FAILED_NO_VALID_SENDER
            return false
        }

        if (!verifySignature()) {
            result = Result.FAILED_NO_VALID_JWS_SIGNATURE
            return false
        }

        if (!verifyPayload()) {
            result = Result.FAILED_NO_VALID_PAYLOAD
            return false
        }

        return true
    }

    private fun isValidJws(jwsList: List<String>): Boolean = jwsList.size == 3

    private fun parseJwsHeader(header: String): Boolean {
        return try {
            val data = JSONObject(String(Base64.decode(header, Base64.DEFAULT))).getJSONArray("x5c")
            val certificateFactory = CertificateFactory.getInstance("X.509")

            for (i in 0 until data.length()) {
                certificates.add(
                    certificateFactory.generateCertificate(Base64.decode(data.getString(i), Base64.DEFAULT).inputStream()) as X509Certificate
                )
            }
            true
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    private fun parseJwsPayload(payloadString: String): Boolean {
        try {
            val data = JSONObject(String(Base64.decode(payloadString, Base64.DEFAULT)))
            payload = Payload(
                nonce = String(Base64.decode(data.getString("nonce"), Base64.DEFAULT)),
                timestamp = data.getLong("timestampMs"),
                packageName = data.getString("apkPackageName"),
                apkDigestSha256 = data.getString("apkDigestSha256"),
                apkCertificateDigestSha256 = parseCertificateDigestSha256(data.getJSONArray("apkCertificateDigestSha256")),
                ctsProfileMatch = data.getBoolean("ctsProfileMatch"),
                basicIntegrity = data.getBoolean("basicIntegrity"),
                advice = ""//advice = data.getString("advice")
            )

            return true
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
    }

    private fun parseCertificateDigestSha256(data: JSONArray): List<String> {
        val certDigests = mutableListOf<String>()
        for (i in 0 until data.length()) {
            certDigests.add(data.getString(i))
        }

        return certDigests
    }

    private fun parseJwsSignature(signature: String) {
        this.signature = signature.toByteArray()
    }

    private fun verifyCertificate(): Boolean {
        // check basic validity of certificate first
        certificates.forEach {
            try {
                it.checkValidity()
            } catch (e: Exception) {
                e.printStackTrace()
                return false
            }
        }

        // check if certificate is known by TrustManager
        try {
            val trustManagerFactory: TrustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
            trustManagerFactory.init(null as KeyStore?)

            trustManagerFactory.trustManagers.forEach {
                if (it is X509TrustManager) {
                    it.checkServerTrusted(certificates.toTypedArray(), "RSA")
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }

        // check certificates CN is from android.attest.com
        return verifyCn(certificates[0])
    }

    private fun verifyCn(certificate: X509Certificate): Boolean {
        val attribute = X500Name(certificates[0].subjectX500Principal.name).getRDNs(BCStyle.CN)[0]
        return attribute.first.value.toString() == ANDROID_ATTEST_CN
    }

    private fun verifySignature(): Boolean {
        try {
            jwsParts?.let { jws ->
                return Signature.getInstance("SHA256withRSA").run {
                    initVerify(certificates[0])
                    update((jws[0] + "." + jws[1]).toByteArray())
                    verify(Base64.decode(jws[2].toByteArray(), Base64.URL_SAFE))
                }
            } ?: return false
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
    }

    private fun verifyPayload(): Boolean {
        return payload?.let {
            var isValid = true

            if (it.nonce != nonce) {
                isValid = false
                logger.d(TAG, "nonce no match")
            }

            if (it.packageName != context.packageName) {
                isValid = false
                logger.d(TAG, "packageName no match")
            }

            if (it.apkDigestSha256 != getApkDigest()) {
                isValid = false
                logger.d(TAG, "apk digest no match")
            }

            if (!verifyApkCertificateDigests()) {
                isValid = false
                logger.d(TAG, "apk certificate digests no match")
            }

            isValid
        } ?: false
    }

    private fun verifyApkCertificateDigests(): Boolean {
        return payload?.let { payload ->
            var isValid = false
            getApkCertificateDigest().forEach {
                isValid = payload.apkCertificateDigestSha256.contains(it)
            }

            isValid
        } ?: false
    }

    private fun getApkDigest(): String =
        try {
            val messageDigest: MessageDigest = MessageDigest.getInstance("SHA-256")
            DigestInputStream(FileInputStream(context.packageCodePath), messageDigest).readBytes()
            Base64.encodeToString(messageDigest.digest(), Base64.NO_WRAP)
        } catch (e: Exception) {
            e.printStackTrace()
            ""
        }

    private fun getApkCertificateDigest(): List<String> {
        val digestList = mutableListOf<String>()

        return try {
            context.packageManager.getPackageInfo(context.packageName, PackageManager.GET_SIGNATURES).signatures.forEach {
                val messageDigest: MessageDigest = MessageDigest.getInstance("SHA-256")
                messageDigest.update(it.toByteArray())
                digestList.add(Base64.encodeToString(messageDigest.digest(), Base64.NO_WRAP))
            }

            digestList
        } catch (e: Exception) {
            e.printStackTrace()
            digestList
        }
    }

    enum class Result {
        INITIAL,
        SUCCESS,
        FAILED_API,
        FAILED_NETWORK,
        FAILED_PLAY_SERVICES,
        FAILED_NO_VALID_JWS_RESPONSE,
        FAILED_NO_VALID_JWS_HEADER,
        FAILED_NO_VALID_PAYLOAD_NOT_READ,
        FAILED_NO_VALID_PAYLOAD,
        FAILED_NO_VALID_SENDER,
        FAILED_NO_VALID_JWS_SIGNATURE
    }

    data class Payload(
        val nonce: String,
        val timestamp: Long,
        val packageName: String,
        val apkDigestSha256: String,
        val apkCertificateDigestSha256: List<String>,
        val ctsProfileMatch: Boolean,
        val basicIntegrity: Boolean,
        val advice: String
    )

    data class AttestationResult(
        val payload: Payload?,
        val result: Result
    )

    companion object {
        const val TAG = "SafetyNetAttestation"
        const val ANDROID_ATTEST_CN = "attest.android.com"
    }
}