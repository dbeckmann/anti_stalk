package com.dbeckmann.privacy.data

import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.model.RootStateEntity

data class DeviceScanResult(
    val installedApps: List<AppInfoEntity>,
    val suspiciousApps: List<AppInfoEntity>,
    val attestationResult: RootStateEntity,
    val safetyNetEnabled: Boolean,
    val hasKeyguardSecured: Boolean,
    val locationEnabledPhotoApps: List<AppInfoEntity>
)