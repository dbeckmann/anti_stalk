package com.dbeckmann.privacy.data

import android.Manifest
import android.app.admin.DevicePolicyManager
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.pm.PermissionInfo
import android.os.Build
import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.model.PermissionEntity
import com.dbeckmann.domain.repository.AppScanRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.ExecutorCallback
import com.dbeckmann.privacy.data.properties.*
import com.dbeckmann.privacy.di.annotation.AppScope
import com.dbeckmann.privacy.util.Logger
import com.dbeckmann.privacy.util.mapOrEmpty
import com.dbeckmann.privacy.view.detail.PermissionMap
import javax.inject.Inject

@AppScope
class AppScan @Inject constructor(
    private val logger: Logger,
    private val context: Context
) : AppScanRepository {

    private val pm = context.packageManager

    private val propertyProcessor = PropertyProcessor(
        SystemFlagProperty(),
        DialerAppProperty(),
        AccessibilityServiceProperty(),
        NotificationListenerServiceProperty(),
        LocationPermissionProperty(),
        InstallerPackageNameProperty(),
        NoCategoryLauncherProperty(),
        OldTargetSdkProperty(),
        ProcessOutgoingCallsProperty(),
        ReadCallLogProperty(),
        ReadSmsProperty(),
        ReadContactProperty(),
        ReceiveSmsProperty(),
        ReadCalendarProperty(),
        RecordAudioProperty(),
        CameraProperty(),
        DeviceAdminProperty(),
        ActiveDeviceAdminProperty(),
        InstallUnknownPackagesProperty(),
        ModifySettingsProperty(),
        SystemAlertWindowProperty()
    )

    override fun getInstalledApps(): Executor<List<AppInfoEntity>> = Executor { executor ->
        logger.d(TAG, "getInstalledApps")

        try {
            executor.onSuccess(getInstalledPackages())
        } catch (e: Exception) {
            executor.onError(e)
        }
    }

    override fun getInstalledAppsWithScores(): Executor<List<AppInfoEntity>> = Executor { executor ->
        try {
            val appInfos = getInstalledPackages()
            val appInfosWithScores = mutableListOf<AppInfoEntity>()
            appInfos.forEach {
                val result = propertyProcessor.calcScore(it)
                val newAppInfo = it.copy(score = result.score, confidenceScore = result.confidenceScore)

                logger.d(TAG, "package ${newAppInfo.packageName} score ${newAppInfo.score}")

                appInfosWithScores.add(newAppInfo)
            }

            executor.onSuccess(appInfosWithScores)
        } catch (e: Exception) {
            executor.onError(e)
        }
    }

    override fun getSuspiciousApps(): Executor<List<AppInfoEntity>> = Executor { executor ->
        try {
            val appInfos = getInstalledPackages()
            val suspiciousAppInfos = mutableListOf<AppInfoEntity>()
            appInfos.forEach {
                val result = propertyProcessor.calcScore(it)
                val newAppInfo = it.copy(score = result.score, confidenceScore = result.confidenceScore)

                logger.d(TAG, "package ${newAppInfo.packageName} score ${newAppInfo.score}")

                if (newAppInfo.confidenceScore >= CONFIDENCE_SCORE_THRESHOLD && !newAppInfo.hasSystemAppFlag) {
                    suspiciousAppInfos.add(newAppInfo)
                }
            }

            executor.onSuccess(suspiciousAppInfos)
        } catch (e: Exception) {
            executor.onError(e)
        }
    }

    override fun getAppInfoEntityByPackageName(packageName: String): Executor<AppInfoEntity> = Executor { executor ->
        try {
            val appInfoEntity = processPackageInfo(packageName)
            val result = propertyProcessor.calcScore(appInfoEntity)
            val newAppInfo = appInfoEntity.copy(score = result.score, confidenceScore = result.confidenceScore)

            executor.onSuccess(newAppInfo)
        } catch (e: Exception) {
            executor.onError(e)
        }
    }

    override fun getAppInfoEntityByListPackageName(packages: List<String>): Executor<List<AppInfoEntity>> = Executor { executor ->
        try {
            val appInfoEntities = mutableListOf<AppInfoEntity>()
            packages.forEach { packageName ->
                getAppInfoEntityByPackageName(packageName).execute(
                    ExecutorCallback(
                        {
                            appInfoEntities.add(it)
                        },
                        {
                            throw it
                        })
                )
            }

            executor.onSuccess(appInfoEntities)
        } catch (e: Exception) {
            executor.onError(e)
        }
    }

    private fun getInstalledPackages(): List<AppInfoEntity> {
        try {
            val packages = pm.getInstalledPackages(0)
            val appInfos = mutableListOf<AppInfoEntity>()

            packages.forEach { info ->
                appInfos.add(processPackageInfo(info.packageName))
            }

            logger.d(TAG, "found ${appInfos.size} installed packages")

            return appInfos
        } catch (e: Exception) {
            throw RuntimeException("could not read packages", e)
        }
    }

    private fun getDetailedPackageInfo(packageName: String): PackageInfo =
        if (Build.VERSION.SDK_INT > 21) {
            pm.getPackageInfo(packageName, DEFAULT_PACKAGE_MANAGER_FLAGS)
        } else {
            pm.getPackageInfo(packageName, DEFAULT_PACKAGE_MANAGER_FLAGS_SDK_21)
        }

    private fun processPackageInfo(packageName: String): AppInfoEntity {
        val info = getDetailedPackageInfo(packageName)

        val dialers = getDialerApps()

        val permissions = info.requestedPermissions.orEmpty().toList()
        val mappedPermissions = permissions.map { createPermissionEntity(it, info.packageName) }
        val servicePermissions = info.services.mapOrEmpty { it.permission }.filterNotNull()
        val receiverPermissions = info.receivers.mapOrEmpty { it.permission }.filterNotNull()

        return AppInfoEntity(
            packageName = info.packageName,
            appName = info.applicationInfo.loadLabel(pm).toString(),
            versionName = info.versionName ?: "unknown",
            targetSdk = info.applicationInfo.targetSdkVersion,
            installerPackageName = pm.getInstallerPackageName(info.packageName),
            firstInstallTime = info.firstInstallTime,
            hasSystemAppFlag = (info.applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM) != 0,
            publicSourceDir = info.applicationInfo.publicSourceDir,

            services = servicePermissions,
            hasAccessibilityService = servicePermissions.contains(Manifest.permission.BIND_ACCESSIBILITY_SERVICE),
            hasNotificationListenerService = servicePermissions.contains(Manifest.permission.BIND_NOTIFICATION_LISTENER_SERVICE),
            hasActiveDeviceAdmin = isActiveDeviceAdmin(info.packageName),
            hasDeviceAdmin = receiverPermissions.contains(Manifest.permission.BIND_DEVICE_ADMIN),
            hasInstallUnknownSource = permissions.contains(Manifest.permission.REQUEST_INSTALL_PACKAGES) || permissions.contains(Manifest.permission.INSTALL_PACKAGES),
            hasModifySystemSettings = permissions.contains(Manifest.permission.WRITE_SETTINGS),

            permissions = mappedPermissions,
            hasLocationFinePermission = permissions.contains(Manifest.permission.ACCESS_COARSE_LOCATION),
            hasLocationCoarsePermission = permissions.contains(Manifest.permission.ACCESS_COARSE_LOCATION),
            hasProcessOutgoingCalls = permissions.contains(Manifest.permission.PROCESS_OUTGOING_CALLS),
            hasReadCallLog = permissions.contains(Manifest.permission.READ_CALL_LOG),
            hasReadPhoneState = permissions.contains(Manifest.permission.READ_PHONE_STATE),
            hasReadContacts = permissions.contains(Manifest.permission.READ_CONTACTS),
            hasReadSms = permissions.contains(Manifest.permission.READ_SMS),
            hasRecordAudio = permissions.contains(Manifest.permission.RECORD_AUDIO),
            hasCamera = permissions.contains(Manifest.permission.CAMERA),
            hasReadCalendar = permissions.contains(Manifest.permission.READ_CALENDAR),
            hasReceiveSms = hasReceiveSms(permissions),
            hasSystemAlertWindow = permissions.contains(Manifest.permission.SYSTEM_ALERT_WINDOW),

            hasUnknownInstallerPackageName = hasUnknownInstallerPackageName(info),
            hasVisibilityToUser = hasLauncherActivityVisibleToUser(info),

            hasDialerAction = dialers.contains(info.packageName)
        )
    }

    private fun createPermissionEntity(permission: String, packageName: String): PermissionEntity {
        return try {
            val permissionInfo = pm.getPermissionInfo(permission, 0)
            val isDangerous = (permissionInfo.protectionLevel and PermissionInfo.PROTECTION_DANGEROUS) != 0
            val isSystem = (permissionInfo.protectionLevel and PermissionInfo.PROTECTION_SIGNATURE) != 0
            val isGranted = (pm.checkPermission(permission, packageName) == PackageManager.PERMISSION_GRANTED)
            val isAndroid = permission.startsWith(ANDROID_PERMISSION_SCHEME) || permission.startsWith(ANDROID_PERMISSION_SCHEME_DEPRECATED)

            PermissionEntity(
                permission,
                group = PermissionMap.PLATFORM_PERMISSIONS[permission],
                isGranted = isGranted,
                isDangerous = isDangerous,
                isSystem = isSystem,
                isAndroid = isAndroid
            )
        } catch (e: Exception) {
            return PermissionEntity(
                permission,
                group = null,
                isGranted = false,
                isSystem = false,
                isDangerous = false,
                isAndroid = false
            )
        }
    }

    private fun hasUnknownInstallerPackageName(packageInfo: PackageInfo): Boolean =
        !InstallerPackageNameProperty.valid_installer_packages.contains(pm.getInstallerPackageName(packageInfo.packageName))

    private fun hasLauncherActivityVisibleToUser(packageInfo: PackageInfo): Boolean =
    // resolving activities in SDK 21 will often lead to TransactionTooLarge exceptions
        // it should be fine to just check for existence of category launcher for this case
        if (Build.VERSION.SDK_INT > 21) {
            hasCategoryLauncher(packageInfo) && !appDisabledLauncherActivity(packageInfo)
        } else {
            hasCategoryLauncher(packageInfo)
        }

    private fun hasCategoryLauncher(packageInfo: PackageInfo): Boolean {
        val launchIntent = pm.getLaunchIntentForPackage(packageInfo.packageName)

        return when {
            launchIntent == null -> false
            launchIntent.categories == null -> false
            !launchIntent.categories.contains(Intent.CATEGORY_LAUNCHER) -> false
            else -> true
        }
    }

    private fun appDisabledLauncherActivity(packageInfo: PackageInfo): Boolean {
        val launchIntent = pm.getLaunchIntentForPackage(packageInfo.packageName)

        return launchIntent?.component?.let {
            when (pm.getComponentEnabledSetting(it)) {
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED -> true
                PackageManager.COMPONENT_ENABLED_STATE_DEFAULT -> {
                    // resolve default value from enabled tag
                    var isDisabled = false

                    for (activityInfo in packageInfo.activities) {
                        val activityComponentName = packageInfo.packageName + "/" + activityInfo.name
                        if (activityComponentName == launchIntent.component!!.flattenToString()) {
                            isDisabled = !activityInfo.isEnabled
                            break
                        }
                    }

                    isDisabled
                }
                else -> false
            }
        } ?: false
    }

    private fun hasReceiveSms(permissions: List<String>): Boolean =
        permissions.contains(Manifest.permission.RECEIVE_SMS) || permissions.contains(Manifest.permission.RECEIVE_MMS)

    private fun getDialerApps(): List<String> {
        val intent = Intent().apply {
            action = Intent.ACTION_DIAL
        }

        val dialers = mutableListOf<String>()
        pm.queryIntentActivities(intent, PackageManager.GET_META_DATA).forEach {
            dialers.add(it.activityInfo.applicationInfo.packageName)
        }

        return dialers
    }

    private fun isActiveDeviceAdmin(packageName: String): Boolean = getActiveDeviceAdmins().contains(packageName)

    private fun getActiveDeviceAdmins(): List<String> {
        val dpm = context.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        return dpm.activeAdmins.mapOrEmpty { it.packageName }
    }

    companion object {
        const val TAG = "AppScan"
        const val ANDROID_PERMISSION_SCHEME = "android.permission"
        const val ANDROID_PERMISSION_SCHEME_DEPRECATED = "com.android"

        const val CONFIDENCE_SCORE_THRESHOLD = 0.7F

        const val DEFAULT_PACKAGE_MANAGER_FLAGS = PackageManager.GET_META_DATA or
                PackageManager.GET_PERMISSIONS or
                PackageManager.GET_RECEIVERS or
                PackageManager.GET_SERVICES or
                PackageManager.GET_INTENT_FILTERS or
                PackageManager.GET_ACTIVITIES or
                PackageManager.GET_DISABLED_COMPONENTS or
                PackageManager.GET_DISABLED_UNTIL_USED_COMPONENTS

        const val DEFAULT_PACKAGE_MANAGER_FLAGS_SDK_21 = PackageManager.GET_META_DATA or
                PackageManager.GET_PERMISSIONS or
                PackageManager.GET_RECEIVERS or
                PackageManager.GET_SERVICES or
                PackageManager.GET_INTENT_FILTERS or
                PackageManager.GET_DISABLED_COMPONENTS or
                PackageManager.GET_DISABLED_UNTIL_USED_COMPONENTS
    }
}