package com.dbeckmann.privacy.data.properties

import com.dbeckmann.domain.model.AppInfoEntity

interface SuspiciousProperty {
    val score: Float
    val isDownRating: Boolean

    fun calcScore(appInfo: AppInfoEntity): Float
}