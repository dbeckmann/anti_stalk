package com.dbeckmann.privacy.data.properties

import com.dbeckmann.domain.model.AppInfoEntity

class InstallerPackageNameProperty : SuspiciousProperty {
    override val score: Float = 1F
    override val isDownRating: Boolean = false

    override fun calcScore(appInfo: AppInfoEntity): Float {
        return if (!appInfo.hasSystemAppFlag && !valid_installer_packages.contains(appInfo.installerPackageName)) {
            score
        } else {
            0F
        }
    }

    companion object {
        val valid_installer_packages = listOf(
            "com.android.vending",
            "com.amazon.venezia"
        )
    }
}