package com.dbeckmann.privacy.data.properties

import com.dbeckmann.domain.model.AppInfoEntity

class ModifySettingsProperty : SuspiciousProperty {
    override val score: Float = 1F
    override val isDownRating: Boolean = false

    override fun calcScore(appInfo: AppInfoEntity): Float =
        if (appInfo.hasModifySystemSettings) {
            score
        } else {
            0F
        }
}