package com.dbeckmann.privacy.data

import android.Manifest
import android.app.KeyguardManager
import android.app.admin.DevicePolicyManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.provider.MediaStore
import android.provider.Settings
import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.model.RootStateEntity
import com.dbeckmann.domain.repository.DeviceScanRepository
import com.dbeckmann.domain.usecase.GetAppInfoEntityByListPackageName
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.privacy.util.Logger
import com.dbeckmann.privacy.util.mapOrEmpty
import com.google.android.gms.safetynet.SafetyNet
import javax.inject.Inject

class DeviceState @Inject constructor(
    private val context: Context,
    private val logger: Logger,
    private val safetyNetAttestation: SafetyNetAttestation,
    private val getAppInfoEntityByListPackageName: GetAppInfoEntityByListPackageName,
    private val playServicesAvailability: PlayServicesAvailabilityRepository
) : DeviceScanRepository {

    override fun getDeviceAdmins(): Executor<List<AppInfoEntity>> = Executor { executor ->
        val dpm = context.getSystemService(Context.DEVICE_POLICY_SERVICE) as DevicePolicyManager
        val activeDeviceAdmins = dpm.activeAdmins.mapOrEmpty { it.packageName }

        getAppInfoEntityByListPackageName.execute(activeDeviceAdmins,
            {
                executor.onSuccess(it)
            },
            {
                executor.onError(it)
            })
    }

    override fun getDeviceRootState(): Executor<RootStateEntity> = Executor { executor ->
        logger.d(TAG, "getDeviceRootState")

        safetyNetAttestation.attest {
            when (it.result) {
                SafetyNetAttestation.Result.SUCCESS -> {
                    if (it.payload?.basicIntegrity == true) {
                        executor.onSuccess(RootStateEntity(RootStateEntity.Result.SUCCESS))
                    } else {
                        executor.onSuccess(RootStateEntity(RootStateEntity.Result.FAILED_ATTESTATION))
                    }
                }
                SafetyNetAttestation.Result.FAILED_PLAY_SERVICES -> executor.onError(SafetyNetNoPlayServicesException())
                else -> executor.onError(Exception("attestation failed: " + it.result))
            }
        }
    }

    override fun hasUsbDebuggingEnabled(): Executor<Boolean> = Executor { executor ->
        val isEnabled = Settings.Secure.getInt(context.contentResolver, Settings.Global.ADB_ENABLED, 0) == 1
        executor.onSuccess(isEnabled)
    }

    override fun hasSafetyNetEnabled(): Executor<Boolean> = Executor { executor ->
        logger.d(TAG, "hasSafetyNetEnabled")

        SafetyNet.getClient(context).isVerifyAppsEnabled
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    executor.onSuccess(it.result?.isVerifyAppsEnabled ?: false)
                }
            }
            .addOnFailureListener {
                executor.onError(it)
            }
    }

    override fun getPhotoAppsWithLocationEnabled(): Executor<List<AppInfoEntity>> = Executor { executor ->
        val appInfos = mutableListOf<AppInfoEntity>()

        // resolve all camera apps on device
        val packages = mutableListOf<String>()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        context.packageManager.queryIntentActivities(intent, PackageManager.GET_META_DATA).apply {
            this.forEach { resolveInfo ->
                packages.add(resolveInfo.activityInfo.packageName)
            }
        }

        // check location and camera permission state for apps
        getAppInfoEntityByListPackageName.execute(packages) {
            it.forEach { appInfo ->
                if (appInfo.hasCameraAndLocationEnabled()) {
                    appInfos.add(appInfo)
                }
            }

            executor.onSuccess(appInfos)
        }
    }

    private fun AppInfoEntity.hasCameraAndLocationEnabled(): Boolean {
        val hasCamera = this.permissions.any { it.name == Manifest.permission.CAMERA }
        val hasLocationEnabled = this.permissions.any {
            (it.name == Manifest.permission.ACCESS_FINE_LOCATION && it.isGranted)
                    || (it.name == Manifest.permission.ACCESS_COARSE_LOCATION && it.isGranted)
        }

        return hasCamera && hasLocationEnabled
    }

    override fun hasKeyguardSecured(): Executor<Boolean> = Executor { executor ->
        val keyguardManager = context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager

        val isSecure = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            keyguardManager.isDeviceSecure
        } else {
            keyguardManager.isKeyguardSecure
        }

        executor.onSuccess(isSecure)
    }

    companion object {
        const val TAG = "DeviceState"
    }
}