package com.dbeckmann.privacy.data.properties

import com.dbeckmann.domain.model.AppInfoEntity

class ActiveDeviceAdminProperty : SuspiciousProperty {
    override val score: Float = 1F
    override val isDownRating: Boolean = false

    override fun calcScore(appInfo: AppInfoEntity): Float =
        if (appInfo.hasActiveDeviceAdmin) {
            score
        } else {
            0F
        }
}