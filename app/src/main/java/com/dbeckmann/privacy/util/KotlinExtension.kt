package com.dbeckmann.privacy.util

fun <T, R> Array<out T>?.mapOrEmpty(transform: (T) -> R): List<R> {
    return this?.map(transform) ?: listOf()
}

fun <T, R> List<out T>?.mapOrEmpty(transform: (T) -> R): List<R> {
    return this?.map(transform) ?: listOf()
}