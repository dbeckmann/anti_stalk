package com.dbeckmann.privacy.util

import android.util.Log

class DefaultLogger : Logger {
    override fun v(tag: String, msg: String): Int = Log.v(tag, msg)
    override fun d(tag: String, msg: String): Int = Log.d(tag, msg)
    override fun i(tag: String, msg: String): Int = Log.i(tag, msg)
    override fun w(tag: String, msg: String): Int = Log.w(tag, msg)
    override fun e(tag: String, msg: String): Int = Log.e(tag, msg)
}