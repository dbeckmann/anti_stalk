package com.dbeckmann.privacy.data

import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.model.RootStateEntity
import com.dbeckmann.domain.repository.DeviceScanRepository
import com.dbeckmann.interactor.usecase.Executor

class MockDeviceScanRepository : DeviceScanRepository {
    override fun getDeviceAdmins(): Executor<List<AppInfoEntity>> = Executor { executor ->
        executor.onSuccess(listOf())
    }

    override fun getDeviceRootState(): Executor<RootStateEntity> = Executor { executor ->
        executor.onSuccess(RootStateEntity(RootStateEntity.Result.SUCCESS))
    }

    override fun hasUsbDebuggingEnabled(): Executor<Boolean> = Executor { executor ->
        executor.onSuccess(false)
    }

    override fun hasSafetyNetEnabled(): Executor<Boolean> = Executor { executor ->
        executor.onSuccess(true)
    }

    override fun getPhotoAppsWithLocationEnabled(): Executor<List<AppInfoEntity>> = Executor { executor ->
        executor.onSuccess(listOf())
    }

    override fun hasKeyguardSecured(): Executor<Boolean> = Executor { executor ->
        executor.onSuccess(true)
    }
}