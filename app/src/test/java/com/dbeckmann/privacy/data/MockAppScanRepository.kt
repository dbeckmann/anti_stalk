package com.dbeckmann.privacy.data

import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.repository.AppScanRepository
import com.dbeckmann.interactor.usecase.Executor

class MockAppScanRepository : AppScanRepository {
    override fun getInstalledApps(): Executor<List<AppInfoEntity>> = Executor { executor ->
        executor.onSuccess(listOf())
    }

    override fun getSuspiciousApps(): Executor<List<AppInfoEntity>> = Executor { executor ->
        executor.onSuccess(listOf())
    }

    override fun getInstalledAppsWithScores(): Executor<List<AppInfoEntity>> = Executor { executor ->
        executor.onSuccess(listOf())
    }

    override fun getAppInfoEntityByPackageName(packageName: String): Executor<AppInfoEntity> = Executor { executor ->
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getAppInfoEntityByListPackageName(packageName: List<String>): Executor<List<AppInfoEntity>> = Executor { executor ->
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}