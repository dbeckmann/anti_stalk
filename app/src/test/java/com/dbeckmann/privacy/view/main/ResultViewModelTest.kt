package com.dbeckmann.privacy.view.main

import com.dbeckmann.privacy.view.result.ResultViewModel
import com.dbeckmann.rxexecutor.RxExecutor
import com.nhaarman.mockitokotlin2.mock
import io.reactivex.schedulers.Schedulers
import org.junit.Test

class ResultViewModelTest {

    private val useCaseExecutor = RxExecutor(Schedulers.single(), Schedulers.single(), "Testing")

    private val mainViewModel = ResultViewModel(
        mock()
    )

    @Test
    fun doDeviceAnalysis_invokes_all_tests() {
        println(mainViewModel.tiles.size)
    }
}