package com.dbeckmann.privacy.view.main.base.tile

import com.dbeckmann.privacy.view.base.tile.Tile
import com.nhaarman.mockitokotlin2.mock
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class TileInterfaceTest {

    private val tile: Tile<String> = mock()

    @Test
    fun tilesAreSame_otherIsNull_resultIsFalse() {
        assertEquals(false, tile.tilesAreSame(null))
    }

    @Test
    fun tilesAreSame_otherIsSame_resultIsTrue() {
        assertEquals(false, tile.tilesAreSame(tile))
    }
}