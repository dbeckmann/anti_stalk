Privacy Check App
=================

Note:
-----

Please note this app is still under development and not yet finished.


About
-----

The purpose of this app is to check all installed apps for certain characteristics that usually spyware or stalkerware apps have.
These apps try to collect privacy related data like device location, user contacts, called phone numbers, notification messages from other apps and in part also recordings of audio.
To be able to do this these apps misuse APIs like NotificationListenerService or AccessibilityService. Also a common characteristic is that these apps try to hide it self from the
users eyes by not including an android.intent.category.LAUNCHER, or to set a target SDK below 23 to prevent dangerous permission checks.

This app collects these, and other, characteristics to calculate a score that in the end is used to identify apps that violate the users privacy.
Beside this, the app so far also does some other checks on the device like SafetyNet attestation, SafetyNet harmful apps check and if Keyguard is secured by PIN or other authentication pattern.

It uses a clean architecture approach to create use cases that will be injected in the components that want to use them through Dagger2.