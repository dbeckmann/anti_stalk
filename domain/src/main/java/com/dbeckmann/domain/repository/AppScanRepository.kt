package com.dbeckmann.domain.repository

import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.interactor.usecase.Executor

interface AppScanRepository {
    fun getInstalledApps(): Executor<List<AppInfoEntity>>
    fun getSuspiciousApps(): Executor<List<AppInfoEntity>>
    fun getInstalledAppsWithScores(): Executor<List<AppInfoEntity>>
    fun getAppInfoEntityByPackageName(packageName: String): Executor<AppInfoEntity>
    fun getAppInfoEntityByListPackageName(packageName: List<String>): Executor<List<AppInfoEntity>>
}