package com.dbeckmann.domain.repository

import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.model.RootStateEntity
import com.dbeckmann.interactor.usecase.Executor

interface DeviceScanRepository {
    fun getDeviceAdmins(): Executor<List<AppInfoEntity>>
    fun getDeviceRootState(): Executor<RootStateEntity>
    fun hasUsbDebuggingEnabled(): Executor<Boolean>
    fun hasSafetyNetEnabled(): Executor<Boolean>
    fun getPhotoAppsWithLocationEnabled(): Executor<List<AppInfoEntity>>
    fun hasKeyguardSecured(): Executor<Boolean>
}