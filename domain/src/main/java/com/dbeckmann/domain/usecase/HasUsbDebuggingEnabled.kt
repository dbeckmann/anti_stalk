package com.dbeckmann.domain.usecase

import com.dbeckmann.domain.repository.DeviceScanRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.NoParamUseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

class HasUsbDebuggingEnabled(
    private val repository: DeviceScanRepository,
    useCaseExecutor: UseCaseExecutor
) : NoParamUseCase<Boolean>(useCaseExecutor) {

    override fun buildUseCase(): Executor<Boolean> = repository.hasUsbDebuggingEnabled()
}