package com.dbeckmann.domain.usecase

import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.repository.DeviceScanRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.NoParamUseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

class GetPhotoAppsWithLocationEnabled(
    private val repository: DeviceScanRepository,
    useCaseExecutor: UseCaseExecutor
) : NoParamUseCase<List<AppInfoEntity>>(useCaseExecutor) {

    override fun buildUseCase(): Executor<List<AppInfoEntity>> = repository.getPhotoAppsWithLocationEnabled()
}