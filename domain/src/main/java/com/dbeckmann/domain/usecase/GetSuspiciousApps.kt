package com.dbeckmann.domain.usecase

import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.repository.AppScanRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.NoParamUseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

class GetSuspiciousApps(
    private val repository: AppScanRepository,
    useCaseExecutor: UseCaseExecutor
) : NoParamUseCase<List<AppInfoEntity>>(useCaseExecutor) {

    override fun buildUseCase(): Executor<List<AppInfoEntity>> = repository.getSuspiciousApps()
}