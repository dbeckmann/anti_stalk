package com.dbeckmann.domain.usecase

import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.repository.DeviceScanRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.NoParamUseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

class HasSafetyNetEnabled(
    private val repository: DeviceScanRepository,
    useCaseExecutor: UseCaseExecutor
) : NoParamUseCase<Boolean>(useCaseExecutor) {

    override fun buildUseCase(): Executor<Boolean> = repository.hasSafetyNetEnabled()
}