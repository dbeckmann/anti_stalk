package com.dbeckmann.domain.usecase

import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.repository.AppScanRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

class GetAppInfoEntityByListPackageName(
    private val repository: AppScanRepository,
    useCaseExecutor: UseCaseExecutor
) : UseCase<List<AppInfoEntity>, List<String>>(useCaseExecutor) {

    override fun buildUseCase(params: List<String>): Executor<List<AppInfoEntity>> {
        return repository.getAppInfoEntityByListPackageName(params)
    }
}