package com.dbeckmann.domain.usecase

import com.dbeckmann.domain.model.RootStateEntity
import com.dbeckmann.domain.repository.DeviceScanRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.NoParamUseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

class GetDeviceRootState(
    private val repository: DeviceScanRepository,
    useCaseExecutor: UseCaseExecutor
) : NoParamUseCase<RootStateEntity>(useCaseExecutor) {

    override fun buildUseCase(): Executor<RootStateEntity> = repository.getDeviceRootState()
}