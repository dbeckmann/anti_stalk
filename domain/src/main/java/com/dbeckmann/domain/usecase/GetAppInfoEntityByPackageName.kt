package com.dbeckmann.domain.usecase

import com.dbeckmann.domain.model.AppInfoEntity
import com.dbeckmann.domain.repository.AppScanRepository
import com.dbeckmann.interactor.usecase.Executor
import com.dbeckmann.interactor.usecase.UseCase
import com.dbeckmann.interactor.usecase.UseCaseExecutor

class GetAppInfoEntityByPackageName(
    private val repository: AppScanRepository,
    useCaseExecutor: UseCaseExecutor
) : UseCase<AppInfoEntity, String>(useCaseExecutor) {

    override fun buildUseCase(params: String): Executor<AppInfoEntity> {
        return repository.getAppInfoEntityByPackageName(params)
    }
}