package com.dbeckmann.domain.model

data class PermissionEntity(
    val name: String,
    val group: String?,
    val isGranted: Boolean,
    val isSystem: Boolean,
    val isDangerous: Boolean,
    val isAndroid: Boolean
)