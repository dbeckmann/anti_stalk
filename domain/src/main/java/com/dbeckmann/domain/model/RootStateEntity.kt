package com.dbeckmann.domain.model

data class RootStateEntity(
    val result: Result
) {

    enum class Result {
        SUCCESS,
        FAILED_ATTESTATION,
        FAILED_API,
        FAILED_PLAY_SERVICES
    }
}