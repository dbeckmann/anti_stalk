fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## Android
### android test
```
fastlane android test
```
Runs all the tests
### android deploy
```
fastlane android deploy
```
Deploy a new version to the Google Play
### android build_debug
```
fastlane android build_debug
```
build a debug .apk
### android build_release
```
fastlane android build_release
```
build a release .apk
### android drive_debug
```
fastlane android drive_debug
```
Uploads debug build to Google Drive folder
### android drive_alpha
```
fastlane android drive_alpha
```
Uploads alpha release build to Google Drive folder
### android drive_all
```
fastlane android drive_all
```
Builds and uploads debug and release build to Google Drive folder
### android release
```
fastlane android release
```
Create a new play store release build

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
